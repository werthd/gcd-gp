package de.dwerth.gpmon.client.run;

import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.util.Properties;

import de.dwerth.gpmon.client.collector.DataCollector;
import de.dwerth.gpmon.client.conn.IWSPrefix;
import de.dwerth.gpmon.client.conn.ServerConnector;
import de.dwerth.gpmon.client.impl.CollectionThread;
import de.dwerth.gpmon.client.impl.MonitoringDataMap;

public class ClientMain {

	private final static String DEFAULT_COLLECTORS = "MemoryCollector,ProcessorCollector,DiskFreeCollector,UptimeCollector,PortCollector,OperatingSystemCollector,NicCollector,DateCollector,HostnameCollector";

	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				ServerConnector.getInstance().close();
			}
		}));
		start();
	}

	@SuppressWarnings("unchecked")
	private static void start() {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("settings.properties"));
		} catch (Exception e1) {
			System.err.println("Settings not found");
		}
		// Connect
		ServerConnector.getInstance();
		String[] classes = props.getProperty("collectors", DEFAULT_COLLECTORS).split(",");
		for (String className : classes) {
			try {
				Class<?> collector = Class.forName("de.dwerth.gpmon.client.collector." + className);
				Constructor<DataCollector> constr = (Constructor<DataCollector>) collector.getDeclaredConstructors()[0];
				DataCollector aCollector = constr.newInstance(new Object[0]);
				CollectionThread ct = new CollectionThread(aCollector);
				ct.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(1500);
		} catch (Exception e) {
		}
		long allocatedMemory = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 / 1024;
		System.out.println("allocatedMemory: " + allocatedMemory + "MB");
		long reportInterval = Long.parseLong(props.getProperty("reportInterval", "1")) * 1000;
		while (true) {
			try {
				String data = MonitoringDataMap.getInstance().getSerializedData();
				ServerConnector.getInstance().sendData(IWSPrefix.DATA, data);
				Thread.sleep(reportInterval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
