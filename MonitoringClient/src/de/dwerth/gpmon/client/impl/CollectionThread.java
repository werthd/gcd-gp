package de.dwerth.gpmon.client.impl;

import java.util.HashMap;

import de.dwerth.gpmon.client.collector.DataCollector;

public class CollectionThread extends Thread {

	private DataCollector collectable;

	public CollectionThread(DataCollector collectable) {
		this.collectable = collectable;
		System.out.println("created collector: " + collectable.getIdentifier());
	}

	@Override
	public void run() {
		while (true) {
			try {
				HashMap<String, String> data = collectable.collectData();
				MonitoringDataMap.getInstance().addData(collectable.getIdentifier(), data);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(collectable.getTimeout());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
