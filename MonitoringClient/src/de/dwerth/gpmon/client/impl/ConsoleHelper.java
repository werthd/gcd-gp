package de.dwerth.gpmon.client.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class ConsoleHelper {

	public static String getConsoleOutput(String command) {
		StringBuilder sb = new StringBuilder();
		try {
			String os = System.getProperty("os.name");
			boolean unix = !os.toLowerCase().contains("windows");
			String suffix = "bat";
			if (unix) {
				suffix = "sh";
			}
			String tmpDir = System.getProperty("java.io.tmpdir");
			File tmpScript = new File(new File(tmpDir),
					System.currentTimeMillis() + "." + suffix);
			if (tmpScript.exists()) {
				tmpScript.delete();
			}
			tmpScript.deleteOnExit();

			ByteArrayInputStream bais = new ByteArrayInputStream(
					command.getBytes());
			FileOutputStream fos = new FileOutputStream(tmpScript);
			for (int read = 0; (read = bais.read()) != -1;) {
				fos.write(read);
			}
			fos.flush();
			fos.close();
			bais.close();

			ProcessBuilder builder = null;
			if (!unix) {
				String newName = "cmd /c " + tmpScript.getName();
				String[] commands = newName.split(" ");
				builder = new ProcessBuilder(commands);
			} else {
				String newName = "bash " + tmpScript.getName();
				String[] commands = newName.split(" ");
				builder = new ProcessBuilder(commands);
			}

			if (tmpDir != null) {
				builder.directory(new File(tmpDir));
			}
			Process p = builder.start();
			InputStream is = p.getInputStream();
			Scanner s = new Scanner(is);
			s.useDelimiter("\\Z");
			if (s.hasNext()) {
				String next = s.next().trim();
				if(!"".equals(next.trim())) {
					sb.append(next);
				}

			}
			s.close();
			is.close();
			if (tmpScript.exists()) {
				tmpScript.delete();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
