package de.dwerth.gpmon.client.impl;

import java.util.HashMap;

import com.google.gson.Gson;

public class MonitoringDataMap {

	private static MonitoringDataMap instance = new MonitoringDataMap();

	private HashMap<String, HashMap<String, String>> data = null;

	private MonitoringDataMap() {
		data = new HashMap<String, HashMap<String, String>>();
	}

	public synchronized void addData(String identifier, HashMap<String, String> actualData) {
		synchronized (data) {
			data.put(identifier, actualData);
		}
	}

	public String getSerializedData() {
		String retVal = "";
		Gson gson = new Gson();
		synchronized (data) {
			retVal = gson.toJson(data);
		}
		return retVal;
	}

	public static MonitoringDataMap getInstance() {
		return instance;
	}
}
