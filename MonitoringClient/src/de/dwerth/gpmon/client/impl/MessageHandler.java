package de.dwerth.gpmon.client.impl;

import org.apache.commons.lang3.StringEscapeUtils;

import de.dwerth.gpmon.client.conn.IWSPrefix;
import de.dwerth.gpmon.client.conn.ServerConnector;

public class MessageHandler {

	public static void parseMessage(String msg) {
		if (msg.length() > 3) {
			String prefix = msg.substring(0, 3);
			String actMsg = msg.substring(3);
			switch (prefix) {
			case IWSPrefix.HEARTBEAT: {
				ServerConnector.getInstance().sendData(prefix, actMsg);
				break;
			}
			case IWSPrefix.REGISTER: {
				System.out.printf("Got message: %s%n", actMsg);
				System.out.println("Closing connection, blocking reconnect for 30 secs");
				ServerConnector.getInstance().setNextConnTs(System.currentTimeMillis() + 30000);
				ServerConnector.getInstance().close();
				break;
			}
			case IWSPrefix.DATA: {
				// Ignore
				break;
			}
			case IWSPrefix.COMMAND:
			case IWSPrefix.COMMANDBASH: {
				System.out.printf("Got msg: %s%n", msg);
				String identifier = actMsg.substring(0, actMsg.indexOf(":"));
				String ret = ConsoleHelper.getConsoleOutput(actMsg.substring(actMsg.indexOf(":") + 1));
				ServerConnector.getInstance().sendData(prefix, identifier + ":" + escapeOutput(ret));
				break;
			}
			default: {
				System.out.println("UNKNOWN MESSAGE TYPE " + prefix);
				break;
			}
			}
		}

	}

	private static String escapeOutput(String input) {
		String retVal = input;
		retVal = StringEscapeUtils.escapeHtml4(retVal);
		retVal = retVal.replace(System.lineSeparator(), "<br>");
		retVal = retVal.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
		return retVal;
	}
}
