package de.dwerth.gpmon.client.conn;

import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.net.URI;
import java.security.KeyStore;
import java.util.Properties;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class ServerConnector {

	private static ServerConnector instance = new ServerConnector();

	private WebsocketClientImpl socket = null;
	private boolean connected = false;
	private long nextConnTs = 0;
	private Socket sslSocket = null;
	private Properties props = new Properties();
	private boolean blockRecreate = false;

	private ServerConnector() {
		try {
			FileInputStream fis = new FileInputStream("settings.properties");
			props.load(fis);
			fis.close();
		} catch (Exception e) {
			System.err.println("settings.properties not found");
		}
		Thread t = new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						if (!connected && !blockRecreate) {
							if (nextConnTs == 0) {
								sslSocket = getSSLSocket();
								String uri = props.getProperty("url");
								connect(uri);
							} else {
								if (System.currentTimeMillis() > nextConnTs) {
									nextConnTs = 0;
								}
							}
						}
						Thread.sleep(5000);
					} catch (Exception e) {

					}
				}
			}
		};
		t.start();
	}

	public void sendData(String prefix, String data) {
		if (connected) {
			socket.send(prefix + data);
		}
	}

	private synchronized void connect(final String destUri) {
		try {
			try {
				URI echoUri = new URI(destUri);
				socket = new WebsocketClientImpl(echoUri);
				if (destUri.startsWith("wss")) {
					socket.setSocket(sslSocket);
				}
				boolean success = socket.connectBlocking();
				System.out.println("Connecting to: " + echoUri + " " + (success ? "SUCCESSFUL" : "FAILED"));
			} catch (InterruptedException e) {
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public static ServerConnector getInstance() {
		return instance;
	}

	public void close() {
		if (connected) {
			socket.close();
			setConnected(false);
		}
	}

	private Socket getSSLSocket() throws Exception {
		// load up the key store
		String storetype = "JKS";
		String keystore = "";
		String storepass = "";
		String keypass = "";
		try {
			keystore = props.getProperty("keystore.path");
			storepass = props.getProperty("keystore.pass");
			keypass = props.getProperty("keystore.keypass");
		} catch (Exception e) {
			System.err.println("Settings not found");
		}

		KeyStore ks = KeyStore.getInstance(storetype);
		File kf = new File(keystore);
		ks.load(new FileInputStream(kf), storepass.toCharArray());

		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(ks, keypass.toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		tmf.init(ks);

		SSLContext sslContext = null;
		sslContext = SSLContext.getInstance("TLS");
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

		SSLSocketFactory factory = sslContext.getSocketFactory();// (SSLSocketFactory) SSLSocketFactory.getDefault();
		return factory.createSocket();
	}

	public boolean isBlockRecreate() {
		return blockRecreate;
	}

	public void setBlockRecreate(boolean blockRecreate) {
		this.blockRecreate = blockRecreate;
	}

	public long getNextConnTs() {
		return nextConnTs;
	}

	public void setNextConnTs(long nextConnTs) {
		this.nextConnTs = nextConnTs;
	}

}
