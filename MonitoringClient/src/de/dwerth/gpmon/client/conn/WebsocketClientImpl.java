package de.dwerth.gpmon.client.conn;

import java.io.FileInputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.java_websocket.handshake.ServerHandshake;

import com.google.gson.Gson;

import de.dwerth.gpmon.client.impl.MessageHandler;

public class WebsocketClientImpl extends org.java_websocket.client.WebSocketClient {

	private String clientIdentifier = null;
	private String username = null;
	private String password = null;

	public WebsocketClientImpl(URI serverURI) {
		super(serverURI);
		clientIdentifier = "unknown";
		try {
			Properties props = new Properties();
			props.load(new FileInputStream("settings.properties"));
			clientIdentifier = props.getProperty("identifier");
			username = props.getProperty("report.user");
			password = props.getProperty("report.pass");
		} catch (Exception e) {
			System.err.println("Settings not found");
		}
	}

	@Override
	public void onClose(int arg0, String arg1, boolean arg2) {
		System.out.println("Closing connection..");
		ServerConnector.getInstance().setConnected(false);
	}

	@Override
	public void onError(Exception arg0) {
		arg0.printStackTrace();
		ServerConnector.getInstance().setConnected(false);
	}

	@Override
	public void onMessage(String msg) {
		MessageHandler.parseMessage(msg);
	}

	@Override
	public void onOpen(ServerHandshake arg0) {
		ServerConnector.getInstance().setBlockRecreate(true);
		Map<String, String> data = new HashMap<>();
		data.put("identifier", clientIdentifier);
		data.put("username", username);
		data.put("password", password);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		send(IWSPrefix.REGISTER + new Gson().toJson(data));
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ServerConnector.getInstance().setConnected(true);
		ServerConnector.getInstance().setBlockRecreate(false);
	}

}
