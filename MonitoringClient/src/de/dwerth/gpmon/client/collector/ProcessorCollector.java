package de.dwerth.gpmon.client.collector;

import java.util.HashMap;
import java.util.StringTokenizer;

public class ProcessorCollector extends ConsoleCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();
		if (isUnix()) {
			String output = getConsoleOutput("top -bn 1 | grep '%Cpu'");
			StringTokenizer st = new StringTokenizer(output, " ");
			st.nextToken(); // %Cpu(s):
			data.put("user", st.nextToken());
			st.nextToken(); // us,
			data.put("system", st.nextToken());
		}
		if (isMac()) {
			String output = getConsoleOutput("top -l 1 | grep 'CPU usage'");
			StringTokenizer st = new StringTokenizer(output, " ");
			st.nextToken(); // CPU
			st.nextToken(); // usage:
			data.put("user", st.nextToken().replace("%", ""));
			st.nextToken(); // user,
			data.put("system", st.nextToken().replace("%", ""));
		}
		if (isWindows()) {
			String[] output = getConsoleOutput("wmic cpu get loadpercentage").split(System.lineSeparator());
			String perct = output[output.length - 1];
			data.put("user", perct);
			data.put("system", "0");
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 1000;
	}

	@Override
	public String getIdentifier() {
		return "processor";
	}

}
