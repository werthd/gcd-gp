package de.dwerth.gpmon.client.collector;

import java.util.HashMap;
import java.util.StringTokenizer;

public class MemoryCollector extends ConsoleCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();
		if (isMac()) {
			String output = getConsoleOutput("top -l 2 | head -n 10 | grep 'PhysMem'");
			StringTokenizer st = new StringTokenizer(output, " ");
			st.nextToken(); // PhysMem:
			data.put("used", st.nextToken().replace("M", ""));
			st.nextToken(); // used
			st.nextToken(); // (XXM
			st.nextToken(); // wired)
			data.put("unused", st.nextToken().replace("M", ""));
		} else if (isUnix()) {
			String output = getConsoleOutput("free -m | grep Mem");
			StringTokenizer st = new StringTokenizer(output, " ");
			st.nextToken(); // Mem:
			st.nextToken(); // total
			data.put("used", st.nextToken());
			data.put("unused", st.nextToken());
		} else if (isWindows()) {
			String[] output = getConsoleOutput("wmic os get freephysicalmemory").split(System.lineSeparator());
			String unused = output[output.length-1];
			output = getConsoleOutput("wmic os get totalvirtualmemorysize").split(System.lineSeparator());
			String total =output[output.length-1];
			int  iUnused = Integer.valueOf(unused.trim());
			int  iTotal = Integer.valueOf(total.trim());
			data.put("used", String.valueOf(iTotal-iUnused));
			data.put("unused", unused.trim());
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 2000;
	}

	@Override
	public String getIdentifier() {
		return "memory";
	}

}
