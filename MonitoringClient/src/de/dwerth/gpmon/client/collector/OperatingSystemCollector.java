package de.dwerth.gpmon.client.collector;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.HashMap;

public class OperatingSystemCollector extends DataCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();

		OperatingSystemMXBean bean = ManagementFactory.getOperatingSystemMXBean();
		data.put("arch", bean.getArch());
		data.put("name", bean.getName());
		data.put("version", bean.getVersion());
		data.put("processors", String.valueOf(bean.getAvailableProcessors()));
		data.put("load", String.valueOf(bean.getSystemLoadAverage()));
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 360000;
	}

	@Override
	public String getIdentifier() {
		return "operatingsystem";
	}

}
