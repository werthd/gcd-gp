package de.dwerth.gpmon.client.collector;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Properties;

public abstract class DataCollector {

	protected Properties props = new Properties();

	public DataCollector() {
		try {
			props.load(new FileInputStream("settings.properties"));
		} catch (Exception e1) {
			System.err.println("Settings not found");
		}
	}

	public abstract HashMap<String, String> collectData();

	public abstract long getDefaultTimeout();

	public abstract String getIdentifier();

	public long getTimeout() {
		return Long.parseLong(props.getProperty("timeout." + getIdentifier(), String.valueOf(getDefaultTimeout())));
	}
}
