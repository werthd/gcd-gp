package de.dwerth.gpmon.client.collector;

import de.dwerth.gpmon.client.impl.ConsoleHelper;

public abstract class ConsoleCollector extends DataCollector {

	private final static String OS = System.getProperty("os.name").toLowerCase();

	public String getConsoleOutput(String command) {
		return ConsoleHelper.getConsoleOutput(command);
	}

	public boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
}
