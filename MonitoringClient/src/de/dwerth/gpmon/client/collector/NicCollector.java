package de.dwerth.gpmon.client.collector;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;

public class NicCollector extends DataCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();

		Enumeration<NetworkInterface> nets;
		try {
			nets = NetworkInterface.getNetworkInterfaces();
			for (NetworkInterface netint : Collections.list(nets)) {
				if (!netint.isLoopback() && !netint.isVirtual() && !netint.isPointToPoint()) {
					HashMap<String, String> dataNic = new HashMap<>();
					dataNic.put("displayname", netint.getDisplayName());
					dataNic.put("up", String.valueOf(netint.isUp()));

					Enumeration<InetAddress> addresses = netint.getInetAddresses();
					List<String> dataAddr = new ArrayList<>();
					while (addresses.hasMoreElements()) {
						InetAddress addr = addresses.nextElement();
						dataAddr.add(addr.getHostAddress());
					}
					dataNic.put("addresses", new Gson().toJson(dataAddr));
					data.put(netint.getName(), new Gson().toJson(dataNic));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 30000;
	}

	@Override
	public String getIdentifier() {
		return "nic";
	}

}
