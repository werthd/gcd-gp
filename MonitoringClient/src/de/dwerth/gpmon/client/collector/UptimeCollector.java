package de.dwerth.gpmon.client.collector;

import java.util.HashMap;

public class UptimeCollector extends ConsoleCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();
		if (isMac() || isUnix()) {
			String output = getConsoleOutput("uptime");
			data.put("uptime", output);
		} else {
			data.put("uptime", "not available for Windows");
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 60000;
	}

	@Override
	public String getIdentifier() {
		return "uptime";
	}

}
