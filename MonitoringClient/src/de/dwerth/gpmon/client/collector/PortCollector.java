package de.dwerth.gpmon.client.collector;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

public class PortCollector extends DataCollector {

	private HashMap<String, Integer> portMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> activePorts = new HashMap<String, Integer>();

	public PortCollector() {
		// well known ports
		portMap.put("ftp", 21);
		portMap.put("ssh", 22);
		portMap.put("telnet", 23);
		portMap.put("dns", 53);
		portMap.put("http", 80);
		portMap.put("ldap", 389);
		portMap.put("https", 443);
		portMap.put("samba", 445);
		portMap.put("smtp", 465);
		portMap.put("afp", 548);
		// registered ports
		portMap.put("openvpn", 1194);
		portMap.put("mssql", 1434);
		portMap.put("cpanel", 2082);
		portMap.put("mysql", 3306);
		portMap.put("vnc", 5900);
		portMap.put("http_alt", 8080);
		portMap.put("vmware", 8222);
		portMap.put("dropbox", 17500);

		try {
			Properties props = new Properties();
			props.load(new FileInputStream("settings.properties"));
			String confPorts = props.getProperty("report.port.ports");
			if (confPorts != null && !"".equals(confPorts)) {
				StringTokenizer st = new StringTokenizer(confPorts, ";");
				while (st.hasMoreTokens()) {
					StringTokenizer st2 = new StringTokenizer(st.nextToken(), ":");
					activePorts.put(st2.nextToken(), Integer.valueOf(st2.nextToken()));
				}
			} else {
				activePorts = portMap;
			}

		} catch (Exception e) {
			activePorts = portMap;
			System.err.println("Settings not found");
		}
	}

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();
		for (String key : activePorts.keySet()) {
			data.put(key, "off");
			Socket sock = null;
			try {
				sock = new Socket("localhost", portMap.get(key));
				data.put(key, "on");
			} catch (Exception e) {
			} finally {
				if (sock != null) {
					try {
						sock.close();
					} catch (IOException e) {
					}
				}
			}
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 5000;
	}

	@Override
	public String getIdentifier() {
		return "port";
	}

}
