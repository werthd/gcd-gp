package de.dwerth.gpmon.client.collector;

import java.util.HashMap;

public class HostnameCollector extends ConsoleCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();

		if (!isWindows()) {
			String output = getConsoleOutput("hostname");
			data.put("hostname", output);
		} else {
			String[] output = getConsoleOutput("hostname").split(System.lineSeparator());
			String hostname = output[output.length - 1];
			data.put("hostname", hostname);
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 60000;
	}

	@Override
	public String getIdentifier() {
		return "hostname";
	}

}
