package de.dwerth.gpmon.client.collector;

import java.util.HashMap;
import java.util.StringTokenizer;

import com.google.gson.Gson;

public class DiskFreeCollector extends ConsoleCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();
		if (isMac()) {
			String[] output = getConsoleOutput("df -h").split(System.lineSeparator());
			for (String aDisk : output) {
				if (aDisk.startsWith("/dev")) {
					HashMap<String, String> dataDisk = new HashMap<String, String>();
					StringTokenizer st = new StringTokenizer(aDisk, " ");
					String filesystem = st.nextToken();
					dataDisk.put("size", st.nextToken());
					dataDisk.put("used", st.nextToken());
					dataDisk.put("avail", st.nextToken());
					dataDisk.put("%used", st.nextToken());
					dataDisk.put("iused", st.nextToken());
					dataDisk.put("ifree", st.nextToken());
					st.nextToken();
					// dataDisk.put("%used", st.nextToken());
					dataDisk.put("mountpoint", st.nextToken());
					data.put(filesystem, new Gson().toJson(dataDisk));
				}
			}
		} else if (isUnix()) {
			String[] output = getConsoleOutput("df -h").split(System.lineSeparator());
			for (String aDisk : output) {
				if (aDisk.startsWith("/dev") || aDisk.startsWith("rootfs")) {
					HashMap<String, String> dataDisk = new HashMap<String, String>();
					StringTokenizer st = new StringTokenizer(aDisk, " ");
					String filesystem = st.nextToken();
					dataDisk.put("size", st.nextToken());
					dataDisk.put("used", st.nextToken());
					dataDisk.put("avail", st.nextToken());
					dataDisk.put("%used", st.nextToken());
					dataDisk.put("mountpoint", st.nextToken());
					data.put(filesystem, new Gson().toJson(dataDisk));
				}
			}
		} else if (isWindows()) {
			HashMap<String, String> dataDisk = new HashMap<String, String>();
			String[] output = getConsoleOutput("fsutil volume diskfree \"c:\"").split(System.lineSeparator());
			int index = 0;
			if (output[0].contains("fsutil")) {
				index++;
			}
			StringTokenizer st = new StringTokenizer(output[index], ":");
			st.nextToken();
			long free = Long.valueOf(st.nextToken().trim()); // free in bytes
			st = new StringTokenizer(output[index + 1], ":");
			st.nextToken();
			long total = Long.valueOf(st.nextToken().trim()); // total in bytes
			long used = total - free;
			dataDisk.put("size", String.valueOf((total / 1024 / 1024)));
			dataDisk.put("used", String.valueOf((used / 1024 / 1024)));
			dataDisk.put("avail", String.valueOf((free / 1024 / 1024)));
			dataDisk.put("%used", String.valueOf(used / (total / 100)) + "%");
			dataDisk.put("mountpoint", "C:/");
			data.put("C:", new Gson().toJson(dataDisk));
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 5000;
	}

	@Override
	public String getIdentifier() {
		return "diskfree";
	}

}
