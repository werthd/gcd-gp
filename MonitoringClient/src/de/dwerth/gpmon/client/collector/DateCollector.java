package de.dwerth.gpmon.client.collector;

import java.util.HashMap;

public class DateCollector extends ConsoleCollector {

	@Override
	public HashMap<String, String> collectData() {
		HashMap<String, String> data = new HashMap<String, String>();

		if (isWindows()) {
			String[] output = getConsoleOutput("date /t").split(System.lineSeparator());
			String date = output[output.length - 1];
			data.put("date", date);
		} else {
			String output = getConsoleOutput("date");
			data.put("date", output);
		}
		return data;
	}

	@Override
	public long getDefaultTimeout() {
		return 1000;
	}

	@Override
	public String getIdentifier() {
		return "date";
	}

}
