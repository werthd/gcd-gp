package de.dwerth.gpmon.server.cache;

import java.util.HashMap;
import java.util.Map;

public class AlertCache {

	private String id = null;
	private String interfaceIdentifier = null;
	private String regex = null;
	private String message = null;
	private String user = null;
	private String action = null;
	private String type = null;
	private String server = null;
	private transient Map<String, Long> lastFired = new HashMap<String, Long>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInterfaceIdentifier() {
		return interfaceIdentifier;
	}

	public void setInterfaceIdentifier(String interfaceIdentifier) {
		this.interfaceIdentifier = interfaceIdentifier;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public long getLastFired(String name) {
		long retVal = 0;
		if (lastFired.get(name) != null) {
			retVal = lastFired.get(name);
		}
		return retVal;
	}

	public void setLastFired(String name, long lastFiredL) {
		lastFired.put(name, lastFiredL);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	@Override
	public String toString() {
		return "AlertCache [id=" + id + ", interfaceIdentifier=" + interfaceIdentifier + ", regex=" + regex + ", message=" + message + ", user=" + user + ", action=" + action + ", type=" + type
				+ ", server=" + server + "]";
	}

	public String prettyPrint() {
		return "Interface='" + interfaceIdentifier + "' Regex='" + regex + "' User='" + user + "' Action='" + action + "' Type='" + type + "'";
	}

}
