package de.dwerth.gpmon.server.filter;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import de.dwerth.gpmon.server.helper.ExceptionHandler;
import de.dwerth.gpmon.server.helper.UserHelper;
import de.dwerth.gpmon.server.servlet.HttpSessionCollector;

public class LoginFilter implements Filter {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(LoginFilter.class);

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		try {
			HttpServletRequest request = (HttpServletRequest) req;
			HttpSession session = request.getSession(true);
			HttpSessionCollector.addIfNotExists(session);

			if (session.getAttribute("role") == null) {
				Cookie[] cookies = request.getCookies();
				if (cookies != null) {
					for (Cookie cookie : cookies) {
						if (cookie.getName().equals("perstsession")) {
							String uuid = cookie.getValue();
							Map<String, Object> userInfo = UserHelper.getLoginUuid(uuid);
							if (userInfo != null) {
								log.debug("Logging user " + userInfo.get("username").toString() + " in by remembering session");
								synchronized (session) {
									for (String key : userInfo.keySet()) {
										session.setAttribute(key, userInfo.get(key));
									}
									// Hashed mail
									session.setAttribute("hashmail", UserHelper.md5(userInfo.get("email").toString().trim().toLowerCase()));
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}

		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {
	}
}
