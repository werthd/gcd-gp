package de.dwerth.gpmon.server.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TimerFilter implements Filter {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(TimerFilter.class);

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		long before = System.currentTimeMillis();
		chain.doFilter(request, response);
		long after = System.currentTimeMillis();
		long total = after - before;
		log.trace("Request to " + request.getRequestURI() + (request.getQueryString() != null ? "/?" + request.getQueryString() : "") + " took " + total + "ms");
	}

	@Override
	public void destroy() {
	}
}
