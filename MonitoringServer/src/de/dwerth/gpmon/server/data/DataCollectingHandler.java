package de.dwerth.gpmon.server.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import de.dwerth.gpmon.server.cache.AlertCache;
import de.dwerth.gpmon.server.conn.IWSPrefix;
import de.dwerth.gpmon.server.conn.WebsocketHandler;
import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.helper.AlertHelper;
import de.dwerth.gpmon.server.helper.ExceptionHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;
import de.dwerth.gpmon.server.servlet.CustomServletContextListener;
import de.dwerth.gpmon.server.threading.ManagedThread;

public class DataCollectingHandler {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(DataCollectingHandler.class);
	private static DataCollectingHandler instance = new DataCollectingHandler();

	private Map<String, Long> accessTimes = new HashMap<String, Long>();
	private Map<String, Long> responseTimes = new HashMap<String, Long>();
	private Map<String, Boolean> persistentInterfaces = new HashMap<String, Boolean>();
	private Map<String, Map<String, Map<String, String>>> allData = new HashMap<String, Map<String, Map<String, String>>>();
	private Map<String, Map<String, AlertCache>> warnings = new HashMap<>();
	private Map<String, Map<String, AlertCache>> errors = new HashMap<>();
	private boolean runBackgroundThreads = true;
	private Thread cleaningThread;
	private Thread responseTimeThread;
	private int warningTimeout = 0;
	private int errorTimeout = 0;
	private long lastSeenWarningTime = 0;

	private Gson gson = new Gson();

	private DataCollectingHandler() {
		ResourceBundle bundle = ResourceBundle.getBundle("settings");
		StringTokenizer st = new StringTokenizer(bundle.getString("persistentInterfaces"), ",");
		while (st.hasMoreTokens()) {
			persistentInterfaces.put(st.nextToken(), Boolean.TRUE);
		}

		final String maxReports = bundle.getString("maxReports");
		final long cleanSleep = Long.valueOf(bundle.getString("clean.sleeptime"));
		warningTimeout = Integer.valueOf(bundle.getString("timeout.warning"));
		errorTimeout = Integer.valueOf(bundle.getString("timeout.error"));
		lastSeenWarningTime = Long.valueOf(bundle.getString("lastseen.warning.time"));

		cleaningThread = new Thread() {
			private static final String CLEANSQL = "DELETE FROM " + DatabaseHandler.TAB_REPORTS + " WHERE id not IN (SELECT id FROM " + DatabaseHandler.TAB_REPORTS
					+ " where interface = '@2' and server = '@1' ORDER BY id DESC LIMIT @3) and interface = '@2' and server = '@1'";

			@Override
			public void run() {
				while (runBackgroundThreads) {
					try {
						List<String> servers = ServerHelper.getConfiguredServerNames();
						for (String server : servers) {
							for (String intName : persistentInterfaces.keySet()) {
								try {
									DatabaseHandler.getInstance().executeRaw(CLEANSQL.replace("@1", server).replace("@2", intName).replace("@3", maxReports));
								} catch (Exception e) {
									ExceptionHandler.handleException(e);
								}
							}
						}
						Thread.sleep(cleanSleep * 1000);
					} catch (InterruptedException e) {

					}
				}
			}
		};
		cleaningThread.start();

		responseTimeThread = new Thread() {
			@Override
			public void run() {
				while (runBackgroundThreads) {
					try {
						WebsocketHandler.getInstance().broadcast(IWSPrefix.HEARTBEAT + String.valueOf(System.currentTimeMillis()));
						Thread.sleep(10000);
					} catch (InterruptedException e) {
					}
				}
			}
		};
		responseTimeThread.start();
	}

	public void collectData(final String servername, final String rawdata) {
		@SuppressWarnings("unused")
		ManagedThread mt = new ManagedThread() {

			@Override
			@SuppressWarnings("unchecked")
			public void doWork() throws Exception {
				long currTs = System.currentTimeMillis();
				synchronized (accessTimes) {
					accessTimes.put(servername, currTs);
				}
				List<AlertCache> alerts = (List<AlertCache>) CustomServletContextListener.context.getAttribute("alerts");
				Map<String, Map<String, String>> data = gson.fromJson(rawdata, HashMap.class);
				synchronized (allData) {
					allData.put(servername, data);
				}

				for (String interf : data.keySet()) {
					String dataRaw = gson.toJson(data.get(interf));
					boolean persistent = false;
					if (persistentInterfaces.containsKey(interf)) {
						persistent = persistentInterfaces.get(interf);
					}

					if (persistent) {
						Map<String, Object> values = new HashMap<String, Object>();
						values.put("server", servername);
						values.put("data", dataRaw);
						values.put("interface", interf);
						values.put("createtime", String.valueOf(currTs));
						DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_REPORTS, values);
					}
					// Alerts
					for (AlertCache cache : alerts) {
						if (interf.equals(cache.getInterfaceIdentifier())) {
							if (cache.getServer() == null || servername.equals(cache.getServer())) {
								if (dataRaw.matches(cache.getRegex())) {
									log.debug("match " + servername + " " + dataRaw + " match " + cache.getRegex());
									AlertHelper.fireAlert(cache, servername);
								}
							}
						}
					}
				}
			}
		};

	}

	public static DataCollectingHandler getInstance() {
		return instance;
	}

	public String getInterfaceData(String identifier, String interfaceName, String key) {
		String retVal = "";
		Map<String, Map<String, String>> data = allData.get(identifier);
		if (data != null) {
			Map<String, String> interfaceData = data.get(interfaceName);
			if (interfaceData != null) {
				retVal = interfaceData.get(key);
				if (retVal == null) {
					retVal = "";
				}
			}
		}
		return retVal;
	}

	public Map<String, String> getInterfaceData(String identifier, String interfaceName) {
		Map<String, String> retVal = null;
		Map<String, Map<String, String>> data = allData.get(identifier);
		if (data != null) {
			retVal = data.get(interfaceName);
		}
		return retVal;
	}

	public String getLastSeen(String server, String pattern) {
		String retVal = "";
		if (accessTimes.containsKey(server)) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			Date d = new Date(accessTimes.get(server));
			retVal = sdf.format(d);
		}
		return retVal;
	}

	public boolean hasWarning(String identifier, String intName) {
		boolean retVal = false;
		Long lastSeen = accessTimes.get(identifier);
		Map<String, AlertCache> map = warnings.get(identifier);
		if (lastSeen != null) {
			long curr = System.currentTimeMillis();
			if ((curr - lastSeen) > (lastSeenWarningTime * 1000)) {
				AlertCache cache = new AlertCache();
				cache.setInterfaceIdentifier(intName);
				cache.setMessage("Server was last seen more than " + lastSeenWarningTime + " seconds ago.");
				cache.setServer(identifier);
				cache.setType("lastseen");
				cache.setLastFired(identifier, curr);
				if (map == null) {
					map = new HashMap<>();
				}
				map.put(intName, cache);
				synchronized (warnings) {
					warnings.put(identifier, map);
				}
			} else {
				if (map != null) {
					AlertCache cache = map.get(intName);
					if (cache != null && "lastseen".equals(cache.getType())) {
						map.remove(intName);
					}
				}
			}
		}

		if (map != null) {
			AlertCache cache = map.get(intName);
			if (cache != null) {
				if (cache.getLastFired(identifier) >= (System.currentTimeMillis() - (warningTimeout * 1000))) {
					retVal = true;
				} else {
					map.remove(intName);
					synchronized (warnings) {
						warnings.put(identifier, map);
					}
				}
			}
		}

		return retVal;
	}

	public boolean hasError(String identifier, String intName) {
		boolean retVal = false;
		Map<String, AlertCache> map = errors.get(identifier);
		if (map != null) {
			AlertCache cache = map.get(intName);
			if (cache != null) {
				if (cache.getLastFired(identifier) >= (System.currentTimeMillis() - (errorTimeout * 1000))) {
					retVal = true;
				} else {
					map.remove(intName);
					synchronized (errors) {
						errors.put(identifier, map);
					}
				}
			}
		}
		return retVal;
	}

	public AlertCache getAlertCacheError(String identifier, String intName) {
		AlertCache retVal = null;
		Map<String, AlertCache> map = errors.get(identifier);
		if (map != null) {
			retVal = map.get(intName);
		}
		return retVal;
	}

	public AlertCache getAlertCacheWarn(String identifier, String intName) {
		AlertCache retVal = null;
		Map<String, AlertCache> map = warnings.get(identifier);
		if (map != null) {
			retVal = map.get(intName);
		}
		return retVal;
	}

	public void addError(String identifier, AlertCache cache) {
		synchronized (errors) {
			Map<String, AlertCache> map = errors.get(identifier);
			if (map == null) {
				map = new HashMap<>();
			}
			map.put(cache.getInterfaceIdentifier(), cache);
			errors.put(identifier, map);
		}
	}

	public void addWarning(String identifier, AlertCache cache) {
		synchronized (warnings) {
			Map<String, AlertCache> map = warnings.get(identifier);
			if (map == null) {
				map = new HashMap<>();
			}
			map.put(cache.getInterfaceIdentifier(), cache);
			warnings.put(identifier, map);
		}
	}

	public void removeErrorsAndWarnings(String identifier, AlertCache cache) {
		synchronized (warnings) {
			Map<String, AlertCache> map = warnings.get(identifier);
			if (map != null) {
				map = new HashMap<>();
				warnings.remove(identifier);
			}
		}
		synchronized (errors) {
			Map<String, AlertCache> map = errors.get(identifier);
			if (map != null) {
				map = new HashMap<>();
				errors.remove(identifier);
			}
		}
	}

	public void setRunBackgroundThreads(boolean runBackgroundThreads) {
		this.runBackgroundThreads = runBackgroundThreads;
		responseTimeThread.interrupt();
		cleaningThread.interrupt();
	}

	public void receiveHeartbeat(String sessionId, String start) {
		long now = System.currentTimeMillis();
		long begin = Long.parseLong(start);
		responseTimes.put(WebsocketHandler.getInstance().getName("r" + sessionId), (now - begin));
	}

	public long getResponseTime(String identifier) {
		long retVal = 0;
		if (responseTimes.containsKey(identifier)) {
			retVal = responseTimes.get(identifier);
		}
		return retVal;
	}

	public void resetData(String servername) {
		synchronized (errors) {
			errors.remove(servername);
		}
		synchronized (warnings) {
			warnings.remove(servername);
		}
		synchronized (responseTimes) {
			responseTimes.remove(servername);
		}
		synchronized (accessTimes) {
			accessTimes.remove(servername);
		}
		synchronized (allData) {
			allData.remove(servername);
		}
	}

}
