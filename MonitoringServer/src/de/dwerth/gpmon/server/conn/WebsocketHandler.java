package de.dwerth.gpmon.server.conn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.Session;

import com.google.gson.Gson;

import de.dwerth.gpmon.server.cache.AlertCache;
import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.helper.AlertHelper;
import de.dwerth.gpmon.server.helper.ExceptionHandler;
import de.dwerth.gpmon.server.helper.UserHelper;
import de.dwerth.gpmon.server.servlet.CustomServletContextListener;

public class WebsocketHandler {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(WebsocketHandler.class);

	private Map<String, Session> sessions = new HashMap<String, Session>();
	private Map<String, String> identifiers = new HashMap<String, String>();

	private static WebsocketHandler instance = new WebsocketHandler();

	private WebsocketHandler() {

	}

	public static WebsocketHandler getInstance() {
		return instance;
	}

	public void broadcast(String msg) {
		for (String key : identifiers.keySet()) {
			if (key.startsWith("r")) {
				sendMessageToId(key, msg);
			}
		}
	}

	public void sendMessageToId(String id, String msg) {
		try {

			Session s = sessions.get(id);
			try {
				if (s != null && s.isOpen()) {
					s.getBasicRemote().sendText(msg);
				}
			} catch (Exception e) {
				synchronized (sessions) {
					log.error("REMOVING " + id);
					remove(id);
					log.error("message that caused exception: " + msg);
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
	}

	public void sendMessage(String rec, String msg) {
		try {
			String id = null;
			for (String anId : identifiers.keySet()) {
				if (identifiers.get(anId).equals(rec)) {
					id = anId;
					break;
				}
			}
			if (id != null) {
				sendMessageToId(id, msg);
			}
		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
	}

	public void put(String sessionId, Session s) {
		synchronized (sessions) {
			sessions.put(sessionId, s);
		}
	}

	@SuppressWarnings("unchecked")
	public void remove(String sessionId) {
		synchronized (sessions) {
			Session s = sessions.get(sessionId);
			sessions.remove(sessionId);
			if (s != null && s.isOpen()) {
				try {
					s.close();
				} catch (Exception e) {
				}
			}
		}
		String name = null;
		synchronized (identifiers) {
			name = identifiers.get(sessionId);
			identifiers.remove(sessionId);
		}
		log.debug("removing " + sessionId + " identifier " + name);
		if (name != null) {
			List<AlertCache> alerts = (List<AlertCache>) CustomServletContextListener.context.getAttribute("alerts");
			for (AlertCache alert : alerts) {
				if (alert.getInterfaceIdentifier().equals("off") && name.equals(alert.getServer())) {
					AlertHelper.fireAlert(alert, name);
				}
			}
			DataCollectingHandler.getInstance().resetData(name);
		}
	}

	@SuppressWarnings("unchecked")
	public String register(String data, String sessionId) {
		String retVal = null;
		Map<String, String> dataMap = new Gson().fromJson(data, Map.class);
		String name = dataMap.get("identifier");
		String username = dataMap.get("username");
		String password = dataMap.get("password");
		if (!identifiers.containsValue(name)) {
			HashMap<String, Object> where = new HashMap<String, Object>();
			where.put("identifier", name);
			List<Map<String, Object>> result = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_SERVERS, null, where);
			if (result.size() > 0) {
				Map<String, Object> userInfo = UserHelper.getLogin(username, password, null);
				if (userInfo != null) {
					log.info("registering " + sessionId + " as " + name);
					synchronized (identifiers) {
						identifiers.put(sessionId, name);
					}
					List<AlertCache> alerts = (List<AlertCache>) CustomServletContextListener.context.getAttribute("alerts");
					for (AlertCache alert : alerts) {
						if (alert.getInterfaceIdentifier().equals("on") && name.equals(alert.getServer())) {
							AlertHelper.fireAlert(alert, name);
						}
					}
				} else {
					retVal = " Wrong user credentials provided";
				}
			} else {
				retVal = name + " is not found in database, wont register";
			}
		} else {
			retVal = " A client with name '" + name + "' is already connected";
		}
		if (retVal != null) {
			log.warn(retVal);
		}
		return retVal;
	}

	public String getName(String id) {
		if (identifiers.containsKey(id)) {
			return identifiers.get(id);
		} else {
			return null;
		}
	}

	public String getId(String identifier) {
		if (identifiers.containsValue(identifier)) {
			for (String key : identifiers.keySet()) {
				if (identifiers.get(key).equals(identifier)) {
					return key;
				}
			}
		}
		return null;

	}
}
