package de.dwerth.gpmon.server.conn;

public interface IWSPrefix {

	public static final String HEARTBEAT = "001";
	public static final String REGISTER = "002";
	public static final String DATA = "003";
	public static final String COMMAND = "004";
	public static final String COMMANDBASH = "005";

}
