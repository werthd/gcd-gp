package de.dwerth.gpmon.server.taglib;

import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.google.gson.Gson;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class DiskfreeTag extends MonitoringTag {

	@SuppressWarnings("unchecked")
	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			try {

				appendWarnings(sb, "diskfree");
				Map<String, String> disks = DataCollectingHandler.getInstance().getInterfaceData(getServer(), "diskfree");
				Gson gson = new Gson();
				for (String filesystem : disks.keySet()) {
					Map<String, String> diskData = gson.fromJson(disks.get(filesystem), Map.class);
					sb.append("<p>" + diskData.get("mountpoint") + " on " + filesystem + "  <b>" + diskData.get("%used") + " used</b> <div class=\"diskfree_progress\"><div style=\"display:none\">"
							+ diskData.get("%used").replace("%", "") + "</div></div></p><br>");
				}
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}

		out.print(sb.toString());
	}

}
