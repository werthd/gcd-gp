package de.dwerth.gpmon.server.taglib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.google.gson.Gson;

import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class ProcessorTag extends MonitoringTag {

	@SuppressWarnings("unchecked")
	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			appendWarnings(sb, "processor");
		}

		sb.append("<script type=\"text/javascript\">function drawChartCpu" + getServer() + "() {");
		if (online) {
			try {
				sb.append("var data = google.visualization.arrayToDataTable([");
				sb.append("['Time', 'User', 'System'],");
				List<Map<String, Object>> res = getHistory();
				Gson gson = new Gson();
				for (int i = 0; i < res.size(); i++) {
					Map<String, Object> map = res.get(i);
					Map<String, String> data = gson.fromJson(map.get("data").toString(), Map.class);
					sb.append("['" + i + "', " + data.get("user") + ", " + data.get("system") + "]");
					if (i != res.size() - 1) {
						sb.append(",");
					}

				}

				sb.append("]);");
				sb.append("var options = {");
				sb.append("title: 'CPU Usage (1 Minute)',");
				sb.append("hAxis: {title: 'Time',  titleTextStyle: {color: '#333'}},");
				sb.append("vAxis: {minValue: 0, maxValue: 100}");
				sb.append("};");

				sb.append("var chart = new google.visualization.AreaChart(document.getElementById('chart_div_cpu_" + getServer() + "'));");
				sb.append("chart.draw(data, options);");
				sb.append("}</script>");
				sb.append("<div id=\"chart_div_cpu_" + getServer() + "\" style=\"width: 300px; height: 250px;\"></div>");
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("}</script>");
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}

		out.print(sb.toString());
	}

	private List<Map<String, Object>> getHistory() {
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("interface", "processor");
		where.put("server", getServer());
		long ts = System.currentTimeMillis() - 60000;
		where.put(">createtime", String.valueOf(ts));
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_REPORTS, new String[] { "data", }, where);
		return res;
	}

}
