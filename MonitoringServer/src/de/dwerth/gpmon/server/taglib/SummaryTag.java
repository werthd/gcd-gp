package de.dwerth.gpmon.server.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class SummaryTag extends MonitoringTag {

	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();
		sb.append("<tr>");
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			sb.append("<td><img src=\"images/icon_online.png\" alt=\"online\" ></td>");
		} else {
			sb.append("<td><img src=\"images/icon_offline.png\" alt=\"offline\" ></td>");
		}
		sb.append("<td>" + getServer() + "</td>");
		sb.append("<td>" + DataCollectingHandler.getInstance().getLastSeen(getServer(), "HH:mm:ss") + "</td>");
		if (online) {
			long responseTime = DataCollectingHandler.getInstance().getResponseTime(getServer());
			sb.append("<td>" + (responseTime > 0 ? String.valueOf(responseTime) + " ms" : "") + "</td>");
		} else {
			sb.append("<td>&nbsp;</td>");
		}
		sb.append("</tr>");
		out.print(sb.toString());
	}

}
