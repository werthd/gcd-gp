package de.dwerth.gpmon.server.taglib;

import javax.servlet.jsp.tagext.SimpleTagSupport;

import de.dwerth.gpmon.server.cache.AlertCache;
import de.dwerth.gpmon.server.data.DataCollectingHandler;

public class MonitoringTag extends SimpleTagSupport {

	protected static String green = "<img src=\"images/dotgreen.png\" alt=\"online\" >";
	protected static String red = "<img src=\"images/dotred.png\" alt=\"error\" >";
	protected static String yellow = "<img src=\"images/dotyellow.png\" alt=\"warning\" >";
	protected static String grey = "<img src=\"images/dotgrey.png\" alt=\"offline\" >";

	protected String server;

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	protected String getWarningDiv(String msg) {
		String retVal = "<div class=\"ui-widget\"><div style=\"margin-top: 20px; padding: 0 .7em;\" class=\"ui-state-highlight ui-corner-all\">"
				+ "<p><span style=\"float: left; margin-right: .3em;\" class=\"ui-icon ui-icon-info\"></span><strong>Warning:</strong> " + msg + "</p></div></div><br>";
		return retVal;
	}

	protected String getErrorDiv(String msg) {
		String retVal = "<div class=\"ui-widget\"><div style=\"padding: 0 .7em;\" class=\"ui-state-error ui-corner-all\"><p>"
				+ "<span style=\"float: left; margin-right: .3em;\" class=\"ui-icon ui-icon-alert\"></span><strong>Error:</strong> " + msg + "</p></div></div>";
		return retVal;
	}

	protected String getStatus(String intName) {
		String retVal = green;
		if (DataCollectingHandler.getInstance().hasWarning(getServer(), intName)) {
			retVal = yellow;
		}
		if (DataCollectingHandler.getInstance().hasError(getServer(), intName)) {
			retVal = red;
		}
		return retVal;
	}

	protected void appendWarnings(StringBuilder sb, String interfaceIdentifier) {
		String status = getStatus(interfaceIdentifier);
		if (yellow.equals(status)) {
			AlertCache cache = DataCollectingHandler.getInstance().getAlertCacheWarn(getServer(), interfaceIdentifier);
			sb.append(getWarningDiv(cache.getMessage()));
		} else if (red.equals(status)) {
			AlertCache cache = DataCollectingHandler.getInstance().getAlertCacheError(getServer(), interfaceIdentifier);
			sb.append(getErrorDiv(cache.getMessage()));
		}
	}
}
