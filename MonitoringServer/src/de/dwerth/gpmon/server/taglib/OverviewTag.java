package de.dwerth.gpmon.server.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import de.dwerth.gpmon.server.helper.ServerHelper;

public class OverviewTag extends MonitoringTag {

	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();
		sb.append("<tr>");
		sb.append("<td>" + getServer() + "</td>");
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			sb.append("<td>" + getStatus("processor") + "</td>");
			sb.append("<td>" + getStatus("memory") + "</td>");
			sb.append("<td>" + getStatus("diskfree") + "</td>");
			sb.append("<td>" + getStatus("nic") + "</td>");
			sb.append("<td>" + getStatus("port") + "</td>");
			sb.append("<td>" + getStatus("uptime") + "</td>");
		} else {
			sb.append("<td>" + grey + "</td>");
			sb.append("<td>" + grey + "</td>");
			sb.append("<td>" + grey + "</td>");
			sb.append("<td>" + grey + "</td>");
			sb.append("<td>" + grey + "</td>");
			sb.append("<td>" + grey + "</td>");
		}

		sb.append("</tr>");
		out.print(sb.toString());
	}

}
