package de.dwerth.gpmon.server.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class DateTag extends MonitoringTag {

	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			try {
				appendWarnings(sb, "date");
				sb.append("<p>" + DataCollectingHandler.getInstance().getInterfaceData(getServer(), "date", "date") + "</p>");
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}

		out.print(sb.toString());
	}

}
