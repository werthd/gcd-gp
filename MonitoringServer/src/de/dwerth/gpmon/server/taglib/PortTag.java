package de.dwerth.gpmon.server.taglib;

import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class PortTag extends MonitoringTag {

	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			try {
				appendWarnings(sb, "port");
				Map<String, String> data = DataCollectingHandler.getInstance().getInterfaceData(getServer(), "port");
				sb.append("<table>");
				sb.append("<tr><th>Service</th><th>Status</th></tr>");
				for (String id : data.keySet()) {
					String status = data.get(id);
					sb.append("<tr><td>" + id + "</td><td><img src=\"images/" + (status.equals("on") ? "dotgreen" : "dotgrey") + ".png\" alt=\"" + status + "\"></td></tr>");
				}
				sb.append("</table>");
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}

		out.print(sb.toString());
	}

}
