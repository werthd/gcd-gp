package de.dwerth.gpmon.server.taglib;

import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.google.gson.Gson;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class NicTag extends MonitoringTag {

	@SuppressWarnings("unchecked")
	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			try {
				appendWarnings(sb, "nic");
				Gson gson = new Gson();
				Map<String, String> data = DataCollectingHandler.getInstance().getInterfaceData(getServer(), "nic");
				for (String name : data.keySet()) {
					Map<String, String> dataNic = gson.fromJson(data.get(name), Map.class);
					boolean up = Boolean.valueOf(dataNic.get("up"));
					sb.append("<p><img src=\"images/" + (up ? "dotgreen" : "dotgrey") + ".png\" alt=\"" + up + "\"> <b>" + name + "</b>");
					if (!name.equals(dataNic.get("displayname"))) {
						sb.append(" (" + dataNic.get("displayname") + ")");
					}
					sb.append("<br>");
					List<String> addresses = gson.fromJson(dataNic.get("addresses"), List.class);
					for (String address : addresses) {
						sb.append(address + "<br>");
					}
					sb.append("</p>");
				}
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}

		out.print(sb.toString());
	}

}
