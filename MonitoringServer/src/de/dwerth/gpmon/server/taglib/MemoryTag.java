package de.dwerth.gpmon.server.taglib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.google.gson.Gson;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class MemoryTag extends MonitoringTag {

	@SuppressWarnings("unchecked")
	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			appendWarnings(sb, "memory");
		}
		sb.append("<script type=\"text/javascript\">function drawChartMem" + getServer() + "() {");
		if (online) {
			sb.append("drawChartMemCur" + getServer() + "();");
			sb.append("drawChartMemAvg" + getServer() + "();");
		}
		sb.append("}</script>");
		if (online) {
			try {
				sb.append("<script type=\"text/javascript\">function drawChartMemCur" + getServer() + "() {");
				sb.append("var data = new google.visualization.DataTable();");
				sb.append("data.addColumn('string', 'Usage');");
				sb.append("data.addColumn('number', 'Percent');");
				String free = DataCollectingHandler.getInstance().getInterfaceData(getServer(), "memory", "unused");
				String used = DataCollectingHandler.getInstance().getInterfaceData(getServer(), "memory", "used");
				sb.append("data.addRows([ [ 'Free', " + free + " ], [ 'Used', " + used + " ] ]);");
				sb.append("var options = {");
				sb.append("'title' : 'Actual Memory Utilization',");
				sb.append("'width' : 300,");
				sb.append("'height' : 250 };");
				sb.append("var chart = new google.visualization.PieChart(document.getElementById('chart_div_memory_cur_" + getServer() + "'));");
				sb.append("chart.draw(data, options);");
				sb.append("}</script>");

				sb.append("<script type=\"text/javascript\">function drawChartMemAvg" + getServer() + "() {");
				sb.append("var data = google.visualization.arrayToDataTable([");
				sb.append("['Time', 'User', 'System'],");
				List<Map<String, Object>> res = getHistory();
				Gson gson = new Gson();
				for (int i = 0; i < res.size(); i++) {
					Map<String, Object> map = res.get(i);
					Map<String, String> data = gson.fromJson(map.get("data").toString(), Map.class);
					sb.append("['" + i + "', " + data.get("used") + ", " + data.get("unused") + "]");
					if (i != res.size() - 1) {
						sb.append(",");
					}

				}

				int max = 10000;
				try {
					max = Integer.valueOf(free) + Integer.valueOf(used);
				} catch (Exception e) {
				}

				sb.append("]);");
				sb.append("var options = {");
				sb.append("title: 'Memory Usage (1 Minute)',");
				sb.append("hAxis: {title: 'Time',  titleTextStyle: {color: '#333'}},");
				sb.append("vAxis: {minValue: 0, maxValue: " + max + "}");
				sb.append("};");

				sb.append("var chart = new google.visualization.AreaChart(document.getElementById('chart_div_memory_avg_" + getServer() + "'));");
				sb.append("chart.draw(data, options);");
				sb.append("}</script>");

				sb.append("<div id=\"chart_div_memory_cur_" + getServer() + "\" style=\"float: left; width: 300px; height: 250px;\"></div>");
				sb.append("<div id=\"chart_div_memory_avg_" + getServer() + "\" style=\"float: left; width: 500px; height: 250px;\"></div>");
				sb.append("<div class=\"clear\"></div>");
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}

		out.print(sb.toString());
	}

	private List<Map<String, Object>> getHistory() {
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("interface", "memory");
		where.put("server", getServer());
		long ts = System.currentTimeMillis() - 60000;
		where.put(">createtime", String.valueOf(ts));
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_REPORTS, new String[] { "data", }, where);
		return res;
	}

}
