package de.dwerth.gpmon.server.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

public class OperatingSystemTag extends MonitoringTag {

	@Override
	public void doTag() throws javax.servlet.jsp.JspException, java.io.IOException {
		PageContext context = (PageContext) getJspContext();
		JspWriter out = context.getOut();
		StringBuilder sb = new StringBuilder();

		boolean header = true;
		if (context.getRequest().getAttribute("header") != null) {
			header = (boolean) context.getRequest().getAttribute("header");
		}
		if (header) {
			sb.append("<h4>" + getServer() + "</h4>");
		}
		boolean online = ServerHelper.isOnline(getServer());
		if (online) {
			try {
				appendWarnings(sb, "operatingsystem");
				String osName = DataCollectingHandler.getInstance().getInterfaceData(getServer(), "operatingsystem", "name");
				String src = "linux.png";
				if (osName.toLowerCase().contains("mac")) {
					src = "mac.png";
				} else if (osName.toLowerCase().contains("win")) {
					src = "windows.png";
				}
				sb.append("<p><img style=\"float:left\" src=\"images/os/" + src + "\"/><b>" + osName + "</b><br>");
				sb.append("Version: " + DataCollectingHandler.getInstance().getInterfaceData(getServer(), "operatingsystem", "version") + "<br>");
				sb.append("Arch: " + DataCollectingHandler.getInstance().getInterfaceData(getServer(), "operatingsystem", "arch") + "<br>");
				sb.append(DataCollectingHandler.getInstance().getInterfaceData(getServer(), "operatingsystem", "processors") + " Processors</p>");
			} catch (Exception e) {
				sb.append("<p>" + getServer() + " has no data for this interface</p>");
			}
		} else {
			sb.append("<p>" + getServer() + " is currently offline.</p>");
		}
		out.print(sb.toString());
	}

}
