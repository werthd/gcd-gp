package de.dwerth.gpmon.server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import de.dwerth.gpmon.server.helper.ExceptionHandler;

public class DatabaseHandler {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(DatabaseHandler.class);
	private static DatabaseHandler instance = new DatabaseHandler();

	public final static String TAB_USERS = "users";
	public final static String TAB_REPORTS = "reports";
	public final static String TAB_SERVERS = "servers";
	public final static String TAB_GROUPS = "groups";
	public final static String TAB_ALERTS = "alerts";
	public final static String TAB_SESSIONS = "sessions";
	public final static String TAB_PASSWORDRESET = "passwordreset";

	private Connection connection;

	private DatabaseHandler() {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("settings");
			try {
				Class.forName(bundle.getString("db.drivername"));
			} catch (ClassNotFoundException e1) {
				ExceptionHandler.handleException(e1);
			}
			// create a database connection
			connection = DriverManager.getConnection(bundle.getString("db.connstring"));
		} catch (Exception e1) {
			ExceptionHandler.handleException(e1);
		}
	}

	public static DatabaseHandler getInstance() {
		return instance;
	}

	public List<Map<String, Object>> getData(String table, String[] columns, Map<String, Object> where) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		PreparedStatement preparedStatement = null;
		synchronized (connection) {

			try {
				// create a database connection
				StringBuilder sql = new StringBuilder();
				sql.append("select ");
				if (columns != null) {
					for (String aCol : columns) {
						sql.append(aCol);
						if (!aCol.equals(columns[columns.length - 1])) {
							sql.append(", ");
						}
					}
				} else {
					sql.append("*");
				}
				sql.append(" from " + table);

				preparedStatement = prepareStatement(sql, where);

				ResultSet rs = preparedStatement.executeQuery();
				ResultSetMetaData rsmd = rs.getMetaData();
				while (rs.next()) {
					HashMap<String, Object> row = new HashMap<String, Object>();
					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						row.put(rsmd.getColumnName(i), rs.getObject(i));
					}
					result.add(row);
				}
				rs.close();
			} catch (Exception e) {
				ExceptionHandler.handleException(e);
			} finally {
				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException e) {
					}
				}
			}
		}
		return result;
	}

	public String deleteData(String table, Map<String, Object> where) {
		String retVal = null;
		PreparedStatement preparedStatement = null;
		synchronized (connection) {
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("delete from " + table);

				preparedStatement = prepareStatement(sql, where);

				boolean exec = preparedStatement.execute();
				log.debug("Execute: " + exec);
			} catch (SQLException e) {
				log.error("SQLE: " + e.getErrorCode());
				retVal = String.valueOf(e.getErrorCode());
				ExceptionHandler.handleException(e);
			} catch (Exception e) {
				ExceptionHandler.handleException(e);
			} finally {
				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException e) {
					}
				}
			}
		}
		return retVal;
	}

	public String insertData(String table, Map<String, Object> values) {
		String retVal = null;
		PreparedStatement preparedStatement = null;
		synchronized (connection) {
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("insert into " + table + " (");

				String[] keys = values.keySet().toArray(new String[0]);
				for (String key : keys) {
					sql.append("\"" + key + "\"");
					if (!key.equals(keys[keys.length - 1])) {
						sql.append(", ");
					}
				}
				sql.append(") values (");
				for (String key : keys) {
					sql.append("?");
					if (!key.equals(keys[keys.length - 1])) {
						sql.append(", ");
					}
				}
				sql.append(")");

				log.debug(sql.toString());
				preparedStatement = connection.prepareStatement(sql.toString());

				int psPos = 1;
				for (String key : keys) {
					Object obj = values.get(key);
					log.debug(psPos + ":" + obj);
					switch (obj.getClass().getSimpleName()) {
					case "String": {
						preparedStatement.setString(psPos, obj.toString());
						break;
					}
					case "Integer": {
						preparedStatement.setInt(psPos, (Integer) obj);
						break;
					}
					default: {
						preparedStatement.setObject(psPos, obj);
						break;
					}
					}

					psPos++;
				}

				boolean exec = preparedStatement.execute();
				log.debug("Execute: " + exec);
			} catch (SQLException e) {
				log.error("SQLE: " + e.getErrorCode());
				retVal = String.valueOf(e.getErrorCode());
				ExceptionHandler.handleException(e);
			} catch (Exception e) {
				ExceptionHandler.handleException(e);
			} finally {
				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException e) {
					}
				}
			}
		}
		return retVal;
	}

	public String updateData(String table, Map<String, Object> values, Map<String, Object> where) {
		String retVal = null;
		PreparedStatement preparedStatement = null;
		synchronized (connection) {
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("update " + table + " set ");

				String[] keys = values.keySet().toArray(new String[0]);
				for (String key : keys) {
					sql.append("\"" + key + "\" = ?");
					if (!key.equals(keys[keys.length - 1])) {
						sql.append(", ");
					}
				}

				if (where != null) {
					keys = where.keySet().toArray(new String[0]);
					sql.append(" where ");
					for (String key : keys) {
						sql.append(key + " = ? ");
						if (!key.equals(keys[keys.length - 1])) {
							sql.append("and ");
						}
					}
				}

				log.debug(sql.toString());
				preparedStatement = connection.prepareStatement(sql.toString());

				int psPos = 1;
				keys = values.keySet().toArray(new String[0]);
				for (String key : keys) {
					Object obj = values.get(key);
					log.debug(psPos + ":" + obj);
					if (obj == null) {
						preparedStatement.setNull(psPos, Types.NULL);
					} else {
						switch (obj.getClass().getSimpleName()) {
						case "String": {
							preparedStatement.setString(psPos, obj.toString());
							break;
						}
						case "Integer": {
							preparedStatement.setInt(psPos, (Integer) obj);
							break;
						}
						default: {
							preparedStatement.setObject(psPos, obj);
							break;
						}
						}
					}
					psPos++;
				}

				if (where != null) {
					keys = where.keySet().toArray(new String[0]);
					for (String key : keys) {
						Object obj = where.get(key);
						log.debug(psPos + ":" + obj);
						switch (obj.getClass().getSimpleName()) {
						case "String": {
							preparedStatement.setString(psPos, obj.toString());
							break;
						}
						case "Integer": {
							preparedStatement.setInt(psPos, (Integer) obj);
							break;
						}
						default: {
							preparedStatement.setObject(psPos, obj);
							break;
						}
						}
						psPos++;
					}
				}

				boolean exec = preparedStatement.execute();
				log.debug("Execute: " + exec);
			} catch (SQLException e) {
				log.error("SQLE: " + e.getErrorCode());
				retVal = String.valueOf(e.getErrorCode());
				ExceptionHandler.handleException(e);
			} catch (Exception e) {
				ExceptionHandler.handleException(e);
			} finally {
				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException e) {
					}
				}
			}
		}
		return retVal;
	}

	public void executeRaw(String sql) {
		PreparedStatement preparedStatement = null;
		synchronized (connection) {
			try {
				preparedStatement = connection.prepareStatement(sql.toString());

				boolean exec = preparedStatement.execute();
				log.debug("Execute: " + exec);
			} catch (SQLException e) {
				log.error("SQLE: " + e.getErrorCode());
				ExceptionHandler.handleException(e);
			} catch (Exception e) {
				ExceptionHandler.handleException(e);
			} finally {
				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException e) {
					}
				}
			}
		}
	}

	private PreparedStatement prepareStatement(StringBuilder sql, Map<String, Object> where) throws Exception {
		PreparedStatement preparedStatement = null;
		if (where != null) {
			sql.append(" where ");
			String[] keys = where.keySet().toArray(new String[0]);
			for (String key : keys) {
				if (key.startsWith(">")) {
					sql.append(key.substring(1) + " > ? ");
				} else if (key.startsWith("<")) {
					sql.append(key.substring(1) + " < ? ");
				} else {
					sql.append(key + " = ? ");
				}

				if (!key.equals(keys[keys.length - 1])) {
					sql.append("and ");
				}
			}

			log.debug(sql.toString());
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setQueryTimeout(5);

			int psPos = 1;
			for (String key : keys) {
				Object obj = where.get(key);
				if (obj == null) {
					preparedStatement.setNull(psPos, Types.NULL);
				} else {
					log.debug(psPos + ":" + obj);
					switch (obj.getClass().getSimpleName()) {
					case "String": {
						preparedStatement.setString(psPos, obj.toString());
						break;
					}
					case "Integer": {
						preparedStatement.setInt(psPos, (Integer) obj);
						break;
					}
					default: {
						preparedStatement.setObject(psPos, obj);
						break;
					}
					}
				}
				psPos++;
			}
		} else {
			log.debug(sql.toString());
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setQueryTimeout(5);
		}
		return preparedStatement;
	}
}
