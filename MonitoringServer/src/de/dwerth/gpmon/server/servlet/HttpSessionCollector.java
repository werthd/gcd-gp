package de.dwerth.gpmon.server.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class HttpSessionCollector implements HttpSessionListener {
	private static final Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		HttpSession session = event.getSession();
		synchronized (sessions) {
			sessions.put(session.getId(), session);
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		sessions.remove(event.getSession().getId());
	}

	public static HttpSession find(String sessionId) {
		return sessions.get(sessionId);
	}

	public static int getSessionCount() {
		return sessions.size();
	}

	public static int getLoginCount() {
		int retVal = 0;
		for (String key : sessions.keySet()) {
			HttpSession session = sessions.get(key);
			if (session.getAttribute("role") != null) {
				retVal++;
			}
		}
		return retVal;
	}

	public static void addIfNotExists(HttpSession session) {
		if (!sessions.containsKey(session.getId())) {
			synchronized (sessions) {
				sessions.put(session.getId(), session);
			}
		}
	}
}