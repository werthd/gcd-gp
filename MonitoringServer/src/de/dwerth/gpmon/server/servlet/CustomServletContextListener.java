package de.dwerth.gpmon.server.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import de.dwerth.gpmon.server.data.DataCollectingHandler;

public class CustomServletContextListener implements ServletContextListener {

	public static ServletContext context = null;

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		DataCollectingHandler.getInstance().setRunBackgroundThreads(false);
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		context = sce.getServletContext();
	}

}
