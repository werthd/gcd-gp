package de.dwerth.gpmon.server.servlet;

import java.io.IOException;
import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.helper.AlertHelper;
import de.dwerth.gpmon.server.helper.ExceptionHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;
import de.dwerth.gpmon.server.helper.UserHelper;

@WebServlet(value = "/ControlServlet", loadOnStartup = 1)
public class ControlServlet extends HttpServlet {

	private static final long serialVersionUID = 7256667326820381165L;
	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ControlServlet.class);

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String action = req.getParameter("action");
		String form = "index.jsp";

		try {
			HttpSession session = req.getSession();

			if (action == null) {
				Cookie[] cookies = req.getCookies();
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals("perstsession")) {
						String uuid = cookie.getValue();
						Map<String, Object> userInfo = UserHelper.getLoginUuid(uuid);
						if (userInfo != null) {
							synchronized (session) {
								for (String key : userInfo.keySet()) {
									session.setAttribute(key, userInfo.get(key));
								}
								// Hashed mail
								session.setAttribute("hashmail", UserHelper.md5(userInfo.get("email").toString().trim().toLowerCase()));
							}
						}
						break;
					}
				}
			} else {
				// Security check
				switch (action) {
				case "addserver":
				case "addgroup":
				case "addalert":
				case "deleteserver":
				case "deletegroup":
				case "deletealert":
				case "editserver": {
					if (!"admin".equals(session.getAttribute("role"))) {
						throw new Exception("Insufficent rights");
					}
					form = "manage.jsp";
					break;
				}
				}

				switch (action) {
				case "login": {
					String username = req.getParameter("username");
					String password = req.getParameter("password");
					String remember = req.getParameter("remember");
					String oauth = req.getParameter("oauth");
					Map<String, Object> userInfo = UserHelper.getLogin(username, password, oauth);
					if (userInfo != null) {
						synchronized (session) {
							for (String key : userInfo.keySet()) {
								session.setAttribute(key, userInfo.get(key));
							}
							// Hashed mail
							session.setAttribute("hashmail", UserHelper.md5(userInfo.get("email").toString().trim().toLowerCase()));
						}

						if ("on".equals(remember)) {
							String uuid = UUID.randomUUID().toString();
							Cookie rememberCookie = new Cookie("perstsession", uuid);
							// 1 Week
							rememberCookie.setMaxAge(7 * 24 * 60 * 60);
							rememberCookie.setHttpOnly(true);
							res.addCookie(rememberCookie);
							// Persist
							UserHelper.persistSession(username, uuid);
						}
					} else {
						ResourceBundle props = ResourceBundle.getBundle("messages");
						req.setAttribute("err-login", props.getString("001"));
					}
					break;
				}
				case "register": {
					String username = req.getParameter("username");
					String password = req.getParameter("password");
					String email = req.getParameter("email");
					String fullname = req.getParameter("fullname");
					String oauth = req.getParameter("oauth");
					try {
						String retVal = UserHelper.register(username, password, email, fullname, "on".equals(oauth));
						if (retVal != null) {
							req.setAttribute("qrurl", retVal);
							form = "oauth.jsp";
						}
					} catch (Exception e) {
						req.setAttribute("err-reg", e.getMessage());
					}
					break;
				}
				case "logout": {
					req.getSession().invalidate();
					Cookie[] cookies = req.getCookies();
					String uuid = null;
					for (Cookie cookie : cookies) {
						if (cookie.getName().equals("perstsession")) {
							cookie.setMaxAge(0);
							uuid = cookie.getValue();
						}
					}
					if (uuid != null) {
						Map<String, Object> where = new HashMap<String, Object>();
						where.put("uuid", uuid);
						DatabaseHandler.getInstance().deleteData(DatabaseHandler.TAB_SESSIONS, where);
					}
					break;
				}
				case "addserver": {
					String identifier = req.getParameter("identifier");
					String group = req.getParameter("group");
					String retVal = ServerHelper.addServer(identifier, group);
					if (retVal != null) {
						req.setAttribute("err-addserver", retVal);
					}
					break;
				}
				case "addgroup": {
					String identifier = req.getParameter("identifier");
					String retVal = ServerHelper.addGroup(identifier);
					if (retVal != null) {
						req.setAttribute("err-addgroup", retVal);
					}
					break;
				}
				case "addalert": {
					String interfaceIdentifier = req.getParameter("interface");
					String regex = req.getParameter("regex");
					String message = req.getParameter("message");
					String user = req.getParameter("user");
					String alertaction = req.getParameter("alertaction");
					String type = req.getParameter("type");
					String server = req.getParameter("server");
					String retVal = AlertHelper.addAlert(interfaceIdentifier, regex, message, user, alertaction, type, server);
					if (retVal != null) {
						req.setAttribute("err-addalert", retVal);
					}
					break;
				}
				case "deleteserver": {
					String identifier = req.getParameter("identifier");
					ServerHelper.deleteServer(identifier);
					break;
				}
				case "deletegroup": {
					String id = req.getParameter("group");
					ServerHelper.deleteGroup(id);
					break;
				}
				case "deletealert": {
					String id = req.getParameter("alert");
					AlertHelper.deleteAlert(id);
					break;
				}
				case "editserver": {
					String identifier = req.getParameter("identifier");
					String group = req.getParameter("group");
					ServerHelper.editServer(identifier, group);
					break;
				}
				case "view": {
					form = "view.jsp";
					String server = req.getParameter("server");
					List<String> servers = new ArrayList<>();
					servers.add(server);
					req.setAttribute("header", Boolean.FALSE);
					req.setAttribute("servers", servers);
					break;
				}
				case "dashboard": {
					form = "index.jsp";
					break;
				}
				case "manage": {
					form = "manage.jsp";
					break;
				}
				case "forgottenpass": {
					String email = req.getParameter("email");
					String key = req.getParameter("key");
					String password = req.getParameter("password");
					String oauth = req.getParameter("oauth");
					form = "forgottenpass.jsp";
					if (email != null) {
						try {
							String retVal = UserHelper.forgottenpassword(email);
							if (retVal != null) {
								req.setAttribute("err-forgotten", retVal);
								form = "forgottenpassreset.jsp";
							}
						} catch (Exception e) {
							req.setAttribute("err-forgotten", e.getMessage());
						}
					} else if (key != null) {
						try {
							String retVal = UserHelper.updatePassword(key, password, "on".equals(oauth));
							if (retVal != null) {
								req.setAttribute("qrurl", retVal);
								form = "oauth.jsp";
							} else {
								form = "index.jsp";
							}
						} catch (Exception e) {
							form = "forgottenpassreset.jsp";
							req.setAttribute("err-forgotten", e.getMessage());
						}
					}
					break;
				}
				default: {
					log.warn("Unknown action: " + action);
					break;
				}
				}
			}
		} catch (Throwable e) {
			ExceptionHandler.handleException(e);
			form = "error.jsp";
		}
		req.getRequestDispatcher(form).include(req, res);
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		log.info("init ControlServlet");
		try {
			Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			getServletContext().setAttribute("servers", ServerHelper.getConfiguredServerNames());
			getServletContext().setAttribute("groups", ServerHelper.getConfiguredGroupNames());
			getServletContext().setAttribute("alerts", AlertHelper.getConfiguredAlerts());
			getServletContext().setAttribute("users", UserHelper.getUserNames());
			getServletContext().setAttribute("groupmapping", ServerHelper.getServerGroupMapping());
		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
	}

}
