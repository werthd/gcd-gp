package de.dwerth.gpmon.server.servlet;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import de.dwerth.gpmon.server.conn.IWSPrefix;
import de.dwerth.gpmon.server.conn.WebsocketHandler;
import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.helper.AlertHelper;
import de.dwerth.gpmon.server.helper.ExceptionHandler;

@ServerEndpoint("/report")
public class ReportServlet {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ReportServlet.class);

	@OnOpen
	public void onOpen(Session session) {
		WebsocketHandler.getInstance().put("r" + session.getId(), session);
	}

	@OnMessage
	public void onMessage(Session session, String msg, boolean last) {
		try {
			if (msg.length() > 3) {
				String prefix = msg.substring(0, 3);
				String actMsg = msg.substring(3);
				switch (prefix) {
				case IWSPrefix.HEARTBEAT: {
					DataCollectingHandler.getInstance().receiveHeartbeat(session.getId(), actMsg);
					break;
				}
				case IWSPrefix.REGISTER: {
					String retMsg = WebsocketHandler.getInstance().register(actMsg, "r" + session.getId());
					if (retMsg != null) {
						log.warn("Register was not successful, closing connection to " + "r" + session.getId());
						WebsocketHandler.getInstance().sendMessageToId("r" + session.getId(), IWSPrefix.REGISTER + retMsg);
						session.close();
					}
					break;
				}
				case IWSPrefix.DATA: {
					String name = WebsocketHandler.getInstance().getName("r" + session.getId());
					if (name != null) {
						DataCollectingHandler.getInstance().collectData(name, actMsg);
					}
					break;
				}
				case IWSPrefix.COMMAND: {
					String identifier = actMsg.substring(0, actMsg.indexOf(":"));
					AlertHelper.sendLater(identifier, actMsg.substring(actMsg.indexOf(":")));
					break;
				}
				case IWSPrefix.COMMANDBASH: {
					String id = actMsg.substring(0, actMsg.indexOf(":"));
					String responseMsg = actMsg.substring(actMsg.indexOf(":") + 1);
					if (!"".equals(responseMsg)) {
						WebsocketHandler.getInstance().sendMessageToId(id, responseMsg);
					}
					break;
				}
				default: {
					log.warn("UNKNOWN MESSAGE TYPE " + prefix);
					break;
				}
				}
			}
		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
	}

	@OnClose
	public void onClose(Session session) {
		WebsocketHandler.getInstance().remove("r" + session.getId());
	}

	@OnError
	public void onError(Session session, Throwable e) {
		WebsocketHandler.getInstance().remove("r" + session.getId());
	}
}
