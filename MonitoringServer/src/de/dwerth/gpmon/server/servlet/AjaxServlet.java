package de.dwerth.gpmon.server.servlet;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import de.dwerth.gpmon.server.conn.IWSPrefix;
import de.dwerth.gpmon.server.conn.WebsocketHandler;
import de.dwerth.gpmon.server.helper.ExceptionHandler;
import de.dwerth.gpmon.server.helper.ServerHelper;

@ServerEndpoint("/ajax")
public class AjaxServlet {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(AjaxServlet.class);

	@OnOpen
	public void onOpen(Session session) {
		WebsocketHandler.getInstance().put("a" + session.getId(), session);
	}

	@OnMessage
	public void onMessage(Session session, String msg, boolean last) {
		StringTokenizer st = new StringTokenizer(msg, ":");
		String jsessionid = st.nextToken();
		String servername = st.nextToken();
		String command = st.nextToken();
		HttpSession httpsession = HttpSessionCollector.find(jsessionid);
		if (httpsession != null && "admin".equals(httpsession.getAttribute("role"))) {
			if (ServerHelper.isOnline(servername)) {
				WebsocketHandler.getInstance().sendMessage(servername, IWSPrefix.COMMANDBASH + "a" + session.getId() + ":" + command);
			} else {
				WebsocketHandler.getInstance().sendMessageToId("a" + session.getId(), servername + " is offline");
			}
		} else {
			WebsocketHandler.getInstance().sendMessageToId(session.getId(), "Unauthorized access.");
			try {
				session.close();
			} catch (IOException e) {
				ExceptionHandler.handleException(e);
			}
			log.warn("Unauthorized access to AjaxServlet, msg: " + msg);
		}
	}

	@OnClose
	public void onClose(Session session) {
		log.debug("onClose: a" + session.getId());
		WebsocketHandler.getInstance().remove("a" + session.getId());
	}

	@OnError
	public void onError(Session session, Throwable e) {
		log.warn("onError: a" + session.getId());
		WebsocketHandler.getInstance().remove("a" + session.getId());
	}
}
