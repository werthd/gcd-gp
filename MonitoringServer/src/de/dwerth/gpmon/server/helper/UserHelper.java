package de.dwerth.gpmon.server.helper;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.UUID;

import org.apache.commons.codec.binary.Base32;

import com.warrenstrange.googleauth.GoogleAuthenticator;

import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.servlet.CustomServletContextListener;

public class UserHelper {

	public static String register(String user, String password, String email, String fullname, boolean useOauth) throws Exception {
		String retVal = null;
		HashMap<String, Object> insert = new HashMap<String, Object>();
		insert.put("username", user);
		String salt = getRandomString();
		insert.put("password", getHashVal(password + salt));
		insert.put("email", email);
		insert.put("name", fullname);
		insert.put("role", "registered");
		insert.put("salt", salt);
		if (useOauth) {
			String secret = getOauthSecret();
			String qrurl = getQRBarcodeURL(user, "davidwerth.de", secret);
			retVal = qrurl;
			insert.put("oauth", secret);
		}

		String result = DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_USERS, insert);
		if (result != null) {
			ResourceBundle props = ResourceBundle.getBundle("messages");
			switch (result) {
			case "19": {
				String msg = props.getString("002");
				throw new Exception(msg);
			}
			}
		} else {
			CustomServletContextListener.context.setAttribute("users", getUserNames());
		}
		return retVal;
	}

	public static Map<String, Object> getLogin(String user, String password, String oauth) {
		Map<String, Object> retVal = null;
		Map<String, Object> row = getLogin(user);
		if (row != null && row.get("password").toString().equals(getHashVal(password + row.get("salt")))) {
			Object rowOauth = row.get("oauth");
			if (rowOauth == null) {
				retVal = row;
			} else {
				if (oauth != null && !"".equals(oauth)) {
					GoogleAuthenticator gAuth = new GoogleAuthenticator();
					boolean isCodeValid = gAuth.authorize(rowOauth.toString(), Integer.valueOf(oauth));
					if (isCodeValid) {
						retVal = row;
					}
				}
			}
		}
		return retVal;
	}

	public static Map<String, Object> getLogin(String user) {
		Map<String, Object> retVal = null;
		HashMap<String, Object> where = new HashMap<String, Object>();
		where.put("username", user);
		List<Map<String, Object>> list = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_USERS, null, where);
		if (list.size() == 1) {
			Map<String, Object> row = list.get(0);
			retVal = row;
		}
		return retVal;
	}

	public static Map<String, Object> getLoginUuid(String uuid) {
		Map<String, Object> retVal = null;
		HashMap<String, Object> where = new HashMap<String, Object>();
		where.put("uuid", uuid);
		List<Map<String, Object>> list = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_SESSIONS, null, where);
		if (list.size() == 1) {
			Map<String, Object> row = list.get(0);
			retVal = getLogin(row.get("username").toString());
		}
		return retVal;
	}

	/**
	 * Hashes the input text
	 * 
	 * @param text
	 * @return SHA-1 hash of input text
	 */
	public static String getHashVal(String text) {
		try {
			MessageDigest cript = MessageDigest.getInstance("SHA-1");
			cript.reset();
			cript.update(text.getBytes("utf8"));
			return byteToHex(cript.digest());
		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
		return "";
	}

	/**
	 * Generate a Salt for passwords
	 * 
	 * @return random string
	 */
	private static String getRandomString() {
		String retVal = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		return retVal;
	}

	private static String byteToHex(byte[] barr) {
		String retVal = "";
		for (byte b : barr) {
			// Returns hex String representation of byte b
			char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
			retVal += new String(array);
		}
		return retVal;
	}

	/**
	 * Used for hashing users email address for gravatar
	 * 
	 * @param message
	 * @return md5 hash of input string
	 */
	public static String md5(String message) {
		String digest = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(message.getBytes("UTF-8"));

			// converting byte array to Hexadecimal String
			StringBuilder sb = new StringBuilder(2 * hash.length);
			for (byte b : hash) {
				sb.append(String.format("%02x", b & 0xff));
			}

			digest = sb.toString();

		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
		return digest;
	}

	public static List<Map<String, Object>> getUserNames() {
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_USERS, new String[] { "username", "name" }, null);
		return res;
	}

	public static void persistSession(String username, String uuid) {
		HashMap<String, Object> insert = new HashMap<String, Object>();
		insert.put("username", username);
		insert.put("uuid", uuid);
		DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_SESSIONS, insert);
	}

	public static String getOauthSecret() {
		int secretSize = 10;
		int numOfScratchCodes = 10;
		int scratchCodeSie = 8;
		// Allocating the buffer
		byte[] buffer = new byte[secretSize + numOfScratchCodes * scratchCodeSie];

		// Filling the buffer with random numbers.
		// Notice: you want to reuse the same random generator
		// while generating larger random number sequences.
		new Random().nextBytes(buffer);

		// Getting the key and converting it to Base32
		Base32 codec = new Base32();
		byte[] secretKey = Arrays.copyOf(buffer, secretSize);
		byte[] bEncodedKey = codec.encode(secretKey);
		String encodedKey = new String(bEncodedKey);
		return encodedKey;
	}

	public static String getQRBarcodeURL(String user, String host, String secret) {
		String format = "https://www.google.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=otpauth://totp/%s@%s%%3Fsecret%%3D%s";
		return String.format(format, user, host, secret);
	}

	public static String forgottenpassword(String email) throws Exception {
		String retVal = "";
		ResourceBundle props = ResourceBundle.getBundle("messages");

		HashMap<String, Object> where = new HashMap<String, Object>();
		where.put("email", email);
		List<Map<String, Object>> list = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_USERS, null, where);

		if (list.size() == 1) {
			Map<String, Object> row = list.get(0);

			where = new HashMap<String, Object>();
			where.put("username", row.get("username"));
			DatabaseHandler.getInstance().deleteData(DatabaseHandler.TAB_PASSWORDRESET, where);

			String key = getRandomString();
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("key", key);
			values.put("username", row.get("username"));
			DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_PASSWORDRESET, values);
			MailHelper.send(email, "Password Reset", "Use the following key to reset your password: " + key);
			retVal = props.getString("005");
		} else {
			throw new Exception(props.getString("004"));
		}
		return retVal;
	}

	public static String updatePassword(String key, String password, boolean oauth) throws Exception {
		String retVal = null;
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("key", key);
		List<Map<String, Object>> list = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_PASSWORDRESET, null, where);
		if (list.size() == 1) {
			DatabaseHandler.getInstance().deleteData(DatabaseHandler.TAB_PASSWORDRESET, where);
			Map<String, Object> row = list.get(0);
			String username = row.get("username").toString();

			where = new HashMap<String, Object>();
			where.put("username", username);
			row = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_USERS, null, where).get(0);
			String salt = row.get("salt").toString();

			Map<String, Object> values = new HashMap<String, Object>();
			values.put("password", getHashVal(password + salt));
			if (oauth) {
				String secret = getOauthSecret();
				String qrurl = getQRBarcodeURL(username, "davidwerth.de", secret);
				retVal = qrurl;
				values.put("oauth", secret);
			} else {
				values.put("oauth", null);
			}
			DatabaseHandler.getInstance().updateData(DatabaseHandler.TAB_USERS, values, where);

		} else {
			throw new Exception("Invalid key");
		}

		return retVal;
	}
}
