package de.dwerth.gpmon.server.helper;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ExceptionHandler.class);

	public static void handleException(Throwable t) {
		log.error("Exeption occurred: ");
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		log.error(sw.toString());
	}
}
