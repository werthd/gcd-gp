package de.dwerth.gpmon.server.helper;

import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.smtp.SMTPTransport;

public class MailHelper {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(MailHelper.class);

	public static void send(String recipientEmail, String title, String message) {
		try {
			if (!recipientEmail.contains("@")) {
				log.warn("Invalid email: " + recipientEmail + " no message sent");
				return;
			}
			ResourceBundle bundle = ResourceBundle.getBundle("mail");
			Properties props = System.getProperties();
			for (String key : bundle.keySet()) {
				props.put(key, bundle.getObject(key));
			}

			Session session = Session.getInstance(props, null);
			final MimeMessage msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress(props.getProperty("mail.smtp.sentfrom")));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));
			msg.setSubject(title);
			msg.setText(message, "utf-8");
			msg.setSentDate(new Date());

			SMTPTransport t = (SMTPTransport) session.getTransport("smtps");
			t.connect(props.getProperty("mail.smtps.host"), props.getProperty("mail.smtps.username"), props.getProperty("mail.smtps.password"));
			t.sendMessage(msg, msg.getAllRecipients());
			t.close();
			log.debug("Email sent to " + recipientEmail);
		} catch (Exception e) {
			ExceptionHandler.handleException(e);
		}
	}

}
