package de.dwerth.gpmon.server.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import de.dwerth.gpmon.server.cache.AlertCache;
import de.dwerth.gpmon.server.conn.IWSPrefix;
import de.dwerth.gpmon.server.conn.WebsocketHandler;
import de.dwerth.gpmon.server.data.DataCollectingHandler;
import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.servlet.CustomServletContextListener;

public class AlertHelper {

	private static Map<String, String> tempMail = new HashMap<>();
	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(AlertHelper.class);
	private static final ResourceBundle bundle = ResourceBundle.getBundle("settings");

	public static String addAlert(String interfaceIdentifier, String regex, String message, String user, String action, String type, String server) {
		String retVal = null;
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("interface", interfaceIdentifier);
		values.put("regex", regex);
		values.put("message", message);
		if (user != null && !"".equals(user)) {
			values.put("user", user);
		}
		if (action != null && !"".equals(action)) {
			values.put("action", action);
		}
		if (type != null && !"".equals(type)) {
			values.put("type", type);
		}
		if (server != null && !"".equals(server)) {
			values.put("server", server);
		}
		String result = DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_ALERTS, values);
		if (result != null) {
			ResourceBundle props = ResourceBundle.getBundle("messages");
			switch (result) {
			case "19": {
				retVal = props.getString("003");
				break;
			}
			}
		} else {
			CustomServletContextListener.context.setAttribute("alerts", getConfiguredAlerts());
		}
		return retVal;
	}

	public static void fireAlert(AlertCache alert, String identifier) {
		long currTs = System.currentTimeMillis();
		long alertTimeout = 60;
		if (bundle.getString("alert.timeout") != null) {
			alertTimeout = Long.valueOf(bundle.getString("alert.timeout"));
		}
		if (alert.getLastFired(identifier) == 0 || currTs - alert.getLastFired(identifier) >= alertTimeout * 1000) {
			log.debug("fire " + identifier + " " + alert);
			alert.setLastFired(identifier, currTs);
			if (alert.getUser() != null) {
				Map<String, Object> where = new HashMap<>();
				where.put("username", alert.getUser());
				Map<String, Object> userData = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_USERS, new String[] { "email" }, where).get(0);
				synchronized (tempMail) {
					tempMail.put(String.valueOf(currTs), userData.get("email").toString() + "|" + "Monitoring: Alert from device " + identifier + "|" + alert.getMessage());
				}

			}
			String action = alert.getAction();
			if (action != null && !"".equals(action.trim()) && !"off".equals(alert.getInterfaceIdentifier())) {
				WebsocketHandler.getInstance().sendMessage(identifier, IWSPrefix.COMMAND + String.valueOf(currTs) + ":" + alert.getAction());
			} else {
				sendLater(String.valueOf(currTs), null);
			}
			String type = alert.getType();
			if (type != null && !"".equals(type.trim())) {
				switch (type) {
				case "error": {
					DataCollectingHandler.getInstance().addError(identifier, alert);
					break;
				}
				case "warning": {
					DataCollectingHandler.getInstance().addWarning(identifier, alert);
					break;
				}
				case "normal": {
					DataCollectingHandler.getInstance().removeErrorsAndWarnings(identifier, alert);
					break;
				}
				}
			}
		}
	}

	public static void sendLater(String identifier, String additionalText) {
		String str = null;
		synchronized (tempMail) {
			str = tempMail.get(identifier);
			tempMail.remove(identifier);
		}
		if (str != null) {
			StringTokenizer st = new StringTokenizer(str, "|");
			MailHelper.send(st.nextToken(), st.nextToken(), st.nextToken() + (additionalText == null ? "" : System.lineSeparator() + additionalText));
		}
	}

	public static List<AlertCache> getConfiguredAlerts() {
		List<AlertCache> retVal = new ArrayList<AlertCache>();
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_ALERTS, null, null);
		for (Map<String, Object> map : res) {
			AlertCache cache = new AlertCache();
			cache.setId(map.get("id").toString());
			cache.setRegex(map.get("regex").toString());
			cache.setMessage(map.get("message").toString());
			cache.setInterfaceIdentifier(map.get("interface").toString());
			if (map.get("user") != null && !"".equals(map.get("user").toString().trim())) {
				cache.setUser(map.get("user").toString());
			}
			if (map.get("action") != null && !"".equals(map.get("action").toString().trim())) {
				cache.setAction(map.get("action").toString());
			}
			if (map.get("type") != null && !"".equals(map.get("type").toString().trim())) {
				cache.setType(map.get("type").toString());
			}
			if (map.get("server") != null && !"".equals(map.get("server").toString().trim())) {
				cache.setServer(map.get("server").toString());
			}
			retVal.add(cache);
		}
		return retVal;
	}

	public static void deleteAlert(String id) {
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("id", id);
		DatabaseHandler.getInstance().deleteData(DatabaseHandler.TAB_ALERTS, where);
		CustomServletContextListener.context.setAttribute("alerts", getConfiguredAlerts());
	}
}
