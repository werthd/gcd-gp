package de.dwerth.gpmon.server.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import de.dwerth.gpmon.server.conn.WebsocketHandler;
import de.dwerth.gpmon.server.database.DatabaseHandler;
import de.dwerth.gpmon.server.servlet.CustomServletContextListener;

public class ServerHelper {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ServerHelper.class);

	public static List<String> getConfiguredServerNames() {
		List<String> retVal = new ArrayList<String>();
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_SERVERS, new String[] { "identifier" }, null);
		for (Map<String, Object> map : res) {
			retVal.add(map.get("identifier").toString());
		}
		return retVal;
	}

	public static Map<String, String> getConfiguredGroupNames() {
		Map<String, String> retVal = new HashMap<String, String>();
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_GROUPS, null, null);
		for (Map<String, Object> part : res) {
			retVal.put(part.get("id").toString(), part.get("name").toString());
		}
		return retVal;
	}

	public static boolean isOnline(String identifier) {
		boolean retVal = false;
		String id = WebsocketHandler.getInstance().getId(identifier);
		if (id != null) {
			retVal = true;
		}
		return retVal;
	}

	public static String addServer(String identifier, String group) {
		String retVal = null;
		log.debug("adding new server: " + identifier);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("identifier", identifier);
		values.put("group", Integer.valueOf(group));
		String result = DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_SERVERS, values);
		if (result != null) {
			ResourceBundle props = ResourceBundle.getBundle("messages");
			switch (result) {
			case "19": {
				retVal = props.getString("003");
				break;
			}
			}
		} else {
			CustomServletContextListener.context.setAttribute("servers", getConfiguredServerNames());
			CustomServletContextListener.context.setAttribute("groupmapping", getServerGroupMapping());

		}
		return retVal;
	}

	public static String addGroup(String name) {
		String retVal = null;
		log.debug("adding new group: " + name);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("name", name);
		String result = DatabaseHandler.getInstance().insertData(DatabaseHandler.TAB_GROUPS, values);
		if (result != null) {
			ResourceBundle props = ResourceBundle.getBundle("messages");
			switch (result) {
			case "19": {
				retVal = props.getString("003");
				break;
			}
			}
		} else {
			CustomServletContextListener.context.setAttribute("groups", ServerHelper.getConfiguredGroupNames());
		}
		return retVal;
	}

	public static void deleteGroup(String id) {
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("id", id);
		DatabaseHandler.getInstance().deleteData(DatabaseHandler.TAB_GROUPS, where);
		CustomServletContextListener.context.setAttribute("groups", ServerHelper.getConfiguredGroupNames());
	}

	public static void deleteServer(String identifier) {
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("identifier", identifier);
		DatabaseHandler.getInstance().deleteData(DatabaseHandler.TAB_SERVERS, where);
		CustomServletContextListener.context.setAttribute("servers", ServerHelper.getConfiguredServerNames());
	}

	public static void editServer(String identifier, String group) {
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("group", Integer.valueOf(group));
		Map<String, Object> where = new HashMap<String, Object>();
		where.put("identifier", identifier);
		DatabaseHandler.getInstance().updateData(DatabaseHandler.TAB_SERVERS, values, where);
		CustomServletContextListener.context.setAttribute("servers", getConfiguredServerNames());
		CustomServletContextListener.context.setAttribute("groupmapping", getServerGroupMapping());
	}

	public static Map<String, List<String>> getServerGroupMapping() {
		Map<String, List<String>> retVal = new HashMap<String, List<String>>();
		Map<String, String> groupNames = getConfiguredGroupNames();
		List<Map<String, Object>> res = DatabaseHandler.getInstance().getData(DatabaseHandler.TAB_SERVERS, null, null);
		for (Map<String, Object> map : res) {
			List<String> lst = retVal.get(groupNames.get(map.get("group").toString()));
			if (lst == null) {
				lst = new ArrayList<>();
			}
			lst.add(map.get("identifier").toString());
			Collections.sort(lst);
			retVal.put(groupNames.get(map.get("group").toString()), lst);
		}
		return retVal;
	}
}
