package de.dwerth.gpmon.server.threading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPool {

	private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ThreadPool.class);

	private int processorCount = 0;
	private List<ManagedThread> threads;
	private AtomicInteger active = new AtomicInteger(0);
	private static final ThreadPool defaultPool = new ThreadPool();

	public ThreadPool() {
		init();
	}

	protected void init() {
		processorCount = Runtime.getRuntime().availableProcessors() * 2;
		log.trace("Creating Threadpool size: " + processorCount);
		threads = new ArrayList<ManagedThread>(processorCount);
	}

	public void add(ManagedThread r) {
		synchronized (threads) {
			threads.add(r);
		}
		next();
	}

	public void terminate() {
		log.trace("Thread ended, size:" + threads.size());
		active.decrementAndGet();
		next();
	}

	private void next() {
		synchronized (threads) {
			if (active.intValue() < processorCount && threads.size() > 0) {
				ManagedThread aThread = threads.get(0);
				threads.remove(0);
				aThread.start();
				log.trace("Starting new Thread, size:" + threads.size());
				active.incrementAndGet();
			}
		}
	}

	public static ThreadPool getDefaultPool() {
		return defaultPool;
	}
}
