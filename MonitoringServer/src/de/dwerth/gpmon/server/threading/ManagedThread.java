package de.dwerth.gpmon.server.threading;

import de.dwerth.gpmon.server.helper.ExceptionHandler;

public abstract class ManagedThread extends Thread {

	private ThreadPool pool;

	public ManagedThread(ThreadPool pool) {
		this.pool = pool;
		pool.add(this);
	}

	public ManagedThread() {
		this.pool = ThreadPool.getDefaultPool();
		pool.add(this);
	}

	@Override
	public void run() {
		try {
			doWork();
		} catch (Throwable e) {
			ExceptionHandler.handleException(e);
		} finally {
			pool.terminate();
		}
	}

	public abstract void doWork() throws Exception;

}
