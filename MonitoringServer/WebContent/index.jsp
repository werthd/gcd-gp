<jsp:include page="incl/header.jspf"></jsp:include>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<body>
<% try { 
if(session.getAttribute("role") != null) {
	%>
<section>
<h2>Overview</h2>
<div class="jqui-tabs" id="tabs-overview">
  <ul>
  	<li><a href="#tabs-0">Overview</a></li>
    <li><a href="#tabs-1">Summary</a></li>
  </ul>
  <div id="tabs-0">
    <jsp:include page="incl/overview.jspf"></jsp:include>
  </div>
  <div id="tabs-1">
    <jsp:include page="incl/summary.jspf"></jsp:include>
  </div>
</div>	
</section>
<br>

<jsp:include page="incl/tab_monitoring.jspf"></jsp:include>
<br>
<%if(session.getAttribute("role").equals("admin")) { %>
<section>
<h2>Tools</h2>
<div class="jqui-tabs" id="tabs-tools">
  <ul>
    <li><a href="#tabs-1">bash</a></li>
  </ul>
  <div id="tabs-1">
    <jsp:include page="incl/bash.jspf"></jsp:include>
  </div>
</div>
</section>

<%
}
	} else {
%>
	
<div class="jqui-tabs" id="tabs">
  <ul>
    <li><a href="#tabs-1">Login</a></li>
    <li><a href="#tabs-2">Register</a></li>
  </ul>
  <div id="tabs-1">
    <jsp:include page="incl/login.jspf"></jsp:include>
  </div>
  <div id="tabs-2">
  	<jsp:include page="incl/register.jspf"></jsp:include>
  </div>
</div>	
<%
} 
} catch(Exception e ) {
 %>
 <jsp:include page="incl/error.jspf"></jsp:include>
 <%
}
%>
<jsp:include page="incl/footer.jspf"></jsp:include>