<jsp:include page="incl/header.jspf"></jsp:include>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<body>
	<%
		try {
			if (session.getAttribute("role") != null) {
	%>
	<jsp:include page="incl/tab_monitoring.jspf"></jsp:include>
	<br>
	<%
		if (session.getAttribute("role").equals("admin")) {
	%>
	<section>
		<h2>Tools</h2>
		<div class="jqui-tabs" id="tabs-tools">
			<ul>
				<li><a href="#tabs-1">bash</a></li>
			</ul>
			<div id="tabs-1">
				<jsp:include page="incl/bash.jspf"></jsp:include>
			</div>
		</div>
	</section>

	<%
		}
			}
		} catch (Exception e) {
	%>
	<jsp:include page="incl/error.jspf"></jsp:include>
	<%
		}
	%>
	<jsp:include page="incl/footer.jspf"></jsp:include>