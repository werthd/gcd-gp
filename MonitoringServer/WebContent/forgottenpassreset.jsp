<jsp:include page="incl/header.jspf"></jsp:include>
<% if (request.getAttribute("err-forgotten") !=null) {
%>
<div class="ui-widget">
	<div style="padding: 0 .7em;" class="ui-state-error ui-corner-all">
		<p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-alert"></span><%= request.getAttribute("err-forgotten")  %></p>
	</div>
</div>
<br>
<%
} %>
<p>Please enter the key you received via Email below.</p>
<form action="ControlServlet" method="post">
<input type="hidden" id="action" name="action" value="forgottenpass">
<p><label for="key">Reset-Key: </label><input id="key" name="key" type="text" required></p>
<p><label for="password">New password: </label><input id="password" name="password" type="password" required></p>
<p><label for="oauth">Use 2-factor-authentication: </label><input id="oauth" name="oauth" type="checkbox"></p>
<p><input type="submit" value="Reset"></p>
</form>
<br>
<p>Back to <a href="ControlServlet">Frontpage</a></p>
<jsp:include page="incl/footer.jspf"></jsp:include>