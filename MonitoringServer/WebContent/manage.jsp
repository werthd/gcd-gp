<jsp:include page="incl/header.jspf"></jsp:include>
<%
	if (session.getAttribute("role").equals("admin")) {
%>
<section>
	<h2>Manage Servers</h2>
	<div class="jqui-tabs" id="tabs-overview">
		<ul>
			<li><a href="#tabs-2">Add Server</a></li>
			<li><a href="#tabs-3">Delete Server</a></li>
			<li><a href="#tabs-4">Edit Server</a></li>
			<li><a href="#tabs-5">Add Group</a></li>
			<li><a href="#tabs-6">Delete Group</a></li>
		</ul>
		<div id="tabs-2">
			<jsp:include page="incl/addserver.jspf"></jsp:include>
		</div>
		<div id="tabs-3">
			<jsp:include page="incl/deleteserver.jspf"></jsp:include>
		</div>
		<div id="tabs-4">
			<jsp:include page="incl/editserver.jspf"></jsp:include>
		</div>
		<div id="tabs-5">
			<jsp:include page="incl/addgroup.jspf"></jsp:include>
		</div>
		<div id="tabs-6">
			<jsp:include page="incl/deletegroup.jspf"></jsp:include>
		</div>
	</div>
</section>
<br>
<section>
	<h2>Alerts &amp; Actions</h2>
	<div class="jqui-tabs" id="tabs-overview">
		<ul>
			<li><a href="#tabs-1">Add Alert</a></li>
			<li><a href="#tabs-2">Delete Alert</a></li>
		</ul>
		<div id="tabs-1">
			<jsp:include page="incl/addalert.jspf"></jsp:include>
		</div>
		<div id="tabs-2">
			<jsp:include page="incl/deletealert.jspf"></jsp:include>
		</div>
	</div>
</section>
<%
	}
%>
<jsp:include page="incl/footer.jspf"></jsp:include>