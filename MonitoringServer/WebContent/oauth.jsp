<jsp:include page="incl/header.jspf"></jsp:include>

<div class="ui-widget">
	<div style="margin-top: 20px; padding: 0 .7em;" class="ui-state-highlight ui-corner-all">
		<p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
		<strong>Caution</strong> Once you activated this QR-Code, it has to be deleted for security reasons afterwards.</p>
	</div>
</div>
<p>Use the following QR-Code for Google Authenticator App.</p>
<a href="<%=request.getAttribute("qrurl") %>" target="_blank"><img src="<%=request.getAttribute("qrurl") %>" alt="QR-Code" /></a>
<br>
<br>
<p>Back to <a href="ControlServlet">Frontpage</a></p>
<jsp:include page="incl/footer.jspf"></jsp:include>