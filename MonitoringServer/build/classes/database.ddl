CREATE TABLE "alerts" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`interface`	TEXT NOT NULL,
	`regex`	TEXT NOT NULL,
	`message`	TEXT NOT NULL,
	`user`	TEXT,
	`action`	TEXT,
	`type`	TEXT,
	`server`	TEXT,
	FOREIGN KEY(`user`) REFERENCES users ( username ),
	FOREIGN KEY(`server`) REFERENCES servers ( identifier )
)
;

CREATE TABLE `groups` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL
)
;

CREATE TABLE `passwordreset` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`username`	TEXT NOT NULL UNIQUE,
	`key`	TEXT NOT NULL,
	FOREIGN KEY(`username`) REFERENCES users(username)
)
;

CREATE TABLE "reports" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`server`	TEXT NOT NULL,
	`data`	TEXT NOT NULL,
	`createtime`	INTEGER NOT NULL,
	`interface`	TEXT NOT NULL,
	FOREIGN KEY(`identifier`) REFERENCES servers(identifier)
)
;

CREATE TABLE "servers" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`identifier`	TEXT NOT NULL UNIQUE,
	`group`	INTEGER NOT NULL DEFAULT 1,
	FOREIGN KEY(`group`) REFERENCES groups(id)
)
;

CREATE TABLE "sessions" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`username`	TEXT NOT NULL,
	`uuid`	TEXT NOT NULL,
	FOREIGN KEY(`username`) REFERENCES users ( username )
)
;

CREATE TABLE "users" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`username`	TEXT NOT NULL UNIQUE,
	`email`	TEXT NOT NULL UNIQUE,
	`password`	TEXT NOT NULL,
	`name`	TEXT NOT NULL,
	`role`	TEXT NOT NULL,
	`salt`	TEXT NOT NULL,
	`oauth`	TEXT UNIQUE
)
;

CREATE INDEX reports_search_ix
ON reports (identifier,interface,createtime)
;

CREATE INDEX servers_identifier_ix
ON servers (identifier)
;

CREATE INDEX users_username_ix
ON users (username)
;

CREATE TRIGGER delete_group_cascade
BEFORE DELETE
ON groups
FOR EACH ROW
BEGIN
UPDATE servers SET 'group' = 1 WHERE servers.'group' = old.id;
END;

CREATE TRIGGER delete_server_cascade
BEFORE DELETE
ON servers
FOR EACH ROW
BEGIN
DELETE FROM reports WHERE reports.server = old.identifier;
END;

CREATE TRIGGER delete_server_cascade2
BEFORE DELETE
ON servers
FOR EACH ROW
BEGIN
DELETE FROM alerts WHERE alerts.server = old.identifier;
END;

CREATE TRIGGER delete_user_cascade
BEFORE DELETE
ON users
FOR EACH ROW
BEGIN
DELETE FROM passwordreset WHERE passwordreset.username = old.username;
END;

CREATE TRIGGER delete_user_cascade2
BEFORE DELETE
ON users
FOR EACH ROW
BEGIN
DELETE FROM sessions WHERE sessions.username = old.username;
END;

CREATE TRIGGER delete_user_cascade3
BEFORE DELETE
ON users
FOR EACH ROW
BEGIN
UPDATE alerts SET 'user' = null WHERE alerts.'user' = old.username;
END;

